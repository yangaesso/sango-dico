# sango-dico
dictionnaire du sango 

## installation

- Install composer globally  https://getcomposer.org/download/
- clone project  
- set database with dump   `sango-dico/sql/dico.sql`
- run `composer install` in project directory
- follow instructions for install
- install assets  `composer install assets --symlink`
- set permission 755 for `var` directory `sudo chmod 755 -R var/`
- set owner www-data for `var` directory  `sudo chown -R www-data:www-data var/`


## useful commands
run these commands in root project directory

- persist database : `php bin/console doctrine:schema:update --force`
- clear cache `php bin/console cache:clear`

## phpmyadmin url
- http://devserver.cloudapp.net/phpmyadmin/index.php

## website url 
- https://bantu-dico.com