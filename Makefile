
run:
    docker-compose up -d
    docker-compose exec php php bin/console cache:clear
    docker-compose exec php php bin/console assets:install
stop:
    docker-compose stop
start:
    docker-compose up -d
down:
    docker-compose down

db:
    docker-compose exec  -T db mysql -uroot -p"arnaud"


warmup: ## Warmup project
	php bin/console cache:clear --no-warmup --env="${env}"; \
	php bin/console cache:warmup --env="${env}"; \
	chmod -Rf 777 ./var;

unit-test: ## Launch unit testing
	./vendor/bin/phpunit --colors=always --exclude-group=exclude

unit-test-coverage-html: ## Export unit coverage in HTML format
	./vendor/bin/phpunit --colors=always --exclude-group=exclude --coverage-html=build/coverage --testdox

