<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/13/18
 * Time: 11:39 PM
 */

namespace AppBundle\Controller;


use FOS\RestBundle\Request\ParamFetcher;


use FOS\RestBundle\Controller\Annotations as Rest;
use MainBundle\Entity\French;
use MainBundle\Entity\FrenchSango;
use MainBundle\Entity\Sango;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;

class FrenchSangoController extends BaseController
{

    /**
     * Deux query strings vont permettre de choisir l’index du premier résultat souhaité (offset) et le nombre de
     * résultats souhaités (limit).
     *
     * @Rest\View(serializerGroups={"frenchSango"})

     * @QueryParam(name="offset", requirements="\d+", default="", description="Index de début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="", description="Index de fin de la pagination")
     * @QueryParam(name="sort", requirements="(asc|desc)", nullable=true, description="Ordre de tri (basé sur le nom)")
     *
     * @Rest\Get("/frenchsango")
     *
     */
    public function getFrenchSangosAction( ParamFetcher $paramFetcher)
    {

        /***
         * Avec le param fetcher, nous pouvons récupérer nos paramètres et les traiter à notre convenance.
         * Pour gérer la pagination avec Doctrine, nous pouvons utiliser le query builder avec les paramètres offset et limit.
         */
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sort = $paramFetcher->get('sort');

        return $this    ->get('doctrine.orm.entity_manager')
                        ->getRepository('MainBundle:FrenchSango')
                        ->findTranslationByPagination($offset,$limit,$sort)   ;
    }


    /**
     *
     * @Rest\View(serializerGroups={"frenchSango"})
     * @Rest\Get("/frenchsango/{id}")
     */
    public function getFrenchSangoAction( Request $request)
    {

        //elastic
        $finder = $this->get("fos_elastica.finder.appfs.french_sango");
        $query = $this->getElasticQuery('id' ,$request->get('id'));

        $frenchSango =  current($finder->find($query));

        if (empty($frenchSango)) {
            return $this->errorMessage('French Sango translation not found');
        }

        return $frenchSango;
    }


    /**
     *
     * @Rest\View(serializerGroups={"frenchSango"})
     * @Rest\Get("/random/frenchsango")
     */
    public function getFrenchSangoRandomAction()
    {

        //elastic
        $finder = $this->get("fos_elastica.finder.appfs.french_sango");
        $query = $this->getElasticQuery('id' ,rand(1,2000));

        $frenchSango =  current($finder->find($query));

        if (empty($frenchSango)) {
            return $this->errorMessage('French Sango translation not found');
        }

        return $frenchSango;
    }


    /**
     *
     * @Rest\View(serializerGroups={"frenchSango"})
     * @Rest\Post("/frenchsango/search")
     */
    public function searchFrenchSangoAction( Request $request)
    {
        $datas = $request->request->all();
        $search = trim($datas['search']);

        //elastic
        $finder = $this->get("fos_elastica.finder.appfs.french_sango");
        $query = $this->getElasticQuery($datas['identifier'],$search);

        $french =  $finder->find($query);

        if (empty($french)) {

            $identifier = substr($datas['identifier'],0,-3);
            $source = $identifier === 'sango' ? $identifier : 'french';
            $target = $source === 'sango' ? 'french' :  'sango';

            $this->setUnknonwWord($source, $target, $search);
            return $this->errorMessage('French Sango  translation not found');
        }

        return $french;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"frenchSango"})
     * @Rest\Post("/frenchsango")
     */
    public function postFrenchSangoAction(Request $request)
    {
        $frenchManager =  $this->get('main.french_manager');
        $sangoManager =  $this->get('main.sango_manager');
        $payload = array_map(
            function ($item){
                return trim(strtolower($item));
                }, $request->request->all()
        );

        $frenchSango  = $this->getTranslation(self::FRENCH_IDENTIFIER, $payload['french'], "fos_elastica.finder.appfs.french_sango");

        if (!empty($frenchSango) && $this->checkIfTranslationExist($frenchSango, $payload['traduction'])) {
            return $this->errorMessage("Translation already exist in database", Response::HTTP_CONFLICT);
        }

        if ($payload['check']) {
            if (!$frenchDictionary = $frenchManager->checkDictionary($payload['french'])) {
                return $this->errorMessage('French word not valid',Response::HTTP_BAD_REQUEST);
            }
        }

        $type = $payload['check'] ? $frenchDictionary->pos : 'noun';
        // on save les mot en database que si ils n'existent pas
        $french = $frenchManager->findTerm($payload['french']);
        if (empty($french)) {
            $french = (new French())
                ->setWord($payload['french'])
                ->setLanguage(French::CODE_ISO)
                ->setType(self::FRENCH_TYPE[$type])
                ->setStatus(true)
                ->setUser($this->getUserApp());
            $this->save($french);
        }
        $sango = $sangoManager->findTerm($payload['traduction']);

        if (empty($sango)) {
            $sango = (new Sango())
                ->setWord($payload['traduction'])
                ->setLanguage(Sango::CODE_ISO)
                ->setType(self::SANGO_TYPE[$type])
                ->setStatus(true)
                ->setUser($this->getUserApp());
            $this->save($sango);
        }

        $frenchSango = (new FrenchSango())
            ->setFrench($french)
            ->setSango($sango)
            ->setStatus(false)
            ->setVotes(0)
            ->setLikes(0)
            ->setUser($this->getUserApp());

        $this->save($frenchSango);

        return $frenchSango;
    }
}