<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/13/18
 * Time: 11:39 PM
 */

namespace AppBundle\Controller;


use FOS\RestBundle\Request\ParamFetcher;
use MainBundle\Form\LingalaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use MainBundle\Entity\Lingala;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


class LingalaController extends BaseController
{

    /**
     * Deux query strings vont permettre de choisir l’index du premier résultat souhaité (offset) et le nombre de
     * résultats souhaités (limit).
     *
     * @Rest\View(serializerGroups={"lingala"})

     * @QueryParam(name="offset", requirements="\d+", default="", description="Index de début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="", description="Index de fin de la pagination")
     * @QueryParam(name="sort", requirements="(asc|desc)", nullable=true, description="Ordre de tri (basé sur le nom)")
     *
     */
    public function getLingalasAction( ParamFetcher $paramFetcher)
    {
        /***
         * Avec le param fetcher, nous pouvons récupérer nos paramètres et les traiter à notre convenance.
         * Pour gérer la pagination avec Doctrine, nous pouvons utiliser le query builder avec les paramètres offset et limit.
         */
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sort = $paramFetcher->get('sort');

        return $this    ->get('doctrine.orm.entity_manager')
                        ->getRepository('MainBundle:Lingala')
                        ->findByPaginattion($offset,$limit,$sort)   ;
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the rewards of an user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Lingala::class, groups={"lingala"}))
     *     )
     * )
     * @SWG\Parameter(
     *     name="order",
     *     in="query",
     *     type="string",
     *     description="The field used to order rewards"
     * )
     * @SWG\Tag(name="lingala")
     *
     * @Rest\View(serializerGroups={"lingala"})
     * @Rest\Get("/lingala/{id}")
     */
    public function getLingalaAction( Request $request)
    {
        $lingala = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Lingala')
            ->find($request->get('id'));

        //elastic
        $finder = $this->get("fos_elastica.finder.appl.lingala");
        $query = $this->getElasticQuery('id' ,$request->get('id'));

        $lingala =  current($finder->find($query));


        /* @var $lingala Lingala */

        if (empty($lingala)) {
            return new JsonResponse(['message' => 'Lingala word not found'], Response::HTTP_NOT_FOUND);
        }

        return $lingala;
    }


    /**
     *
     * @Rest\View(serializerGroups={"lingala"})
     * @Rest\Post("/lingala/search")
     */
    public function searchLingalaAction( Request $request)
    {
        $datas = $request->request->all();
        $finder = $this->get("fos_elastica.finder.appl.lingala");

        return $finder->find($datas['search'].'*');
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"lingala"})
     * @Rest\Post("/lingala")
     */
    public function postLingalaAction(Request $request)
    {
        $lingala = new Lingala();

        $payload = $request->request->all();

        if($this->checkIfWordRExist("fos_elastica.finder.appl.lingala", $payload['word'])){
            return $this->errorMessage("Word already exist in database");
        };

        $form = $this->createForm(LingalaType::class, $lingala);

        /***
         * Donc pour mieux répondre aux contraintes REST, au lieu d’utiliser la méthode handleRequest pour soumettre
         * le formulaire, nous avons opté pour la soumission manuelle avec submit. Nous adaptons Symfony à REST et pas l’inverse.
         */
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($lingala);
            $em->flush();
            return $lingala;
        } else {
            return $form;
        }
    }

    /**
     * @Rest\View(serializerGroups={"lingala"})
     * @Rest\Put("/lingala/{id}")
     */
    public function updateLingalaAction(Request $request)
    {
        $lingala = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Lingala')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire


        /* @var $lingala Lingala */
        if (empty($lingala)) {
            $this->lingalaNotFound();
        }

        $form = $this->createForm(LingalaType::class, $lingala);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($lingala);
            $em->flush();
            return $lingala;
        } else {
            return $form;
        }

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/lingala/{id}")
     */
    public function removeLingalaAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $lingala = $em->getRepository('MainBundle:Lingala')
            ->find($request->get('id'));
        /* @var $lingala Lingala */

        // Une action idempotente est une action qui produit le même résultat et ce, peu importe le nombre de fois qu’elle est exécutée.
        if ($lingala){
            $em->remove($lingala);
            $em->flush();
        }
    }

    /**
     * @Rest\View()
     * @Rest\Put("/lingala/{id}")
     */
    public function updatePlaceAction(Request $request)
    {
        $lingala = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Lingala')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $lingala Lingala */

        if (empty($lingala)) {
            return new JsonResponse(['message' => 'Lingala not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(LingalaType::class, $lingala);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($lingala);
            $em->flush();
            return $lingala;
        } else {
            return $form;
        }

    }

    /***
     * @return static
     *  Au lieu de renvoyer une réponse JSON, nous allons juste renvoyer une vue FOSRestBundle et laisser le view handler le formater en JSON
     */
    private function lingalaNotFound()
    {
        return \FOS\RestBundle\View\View::create(['message' => 'Fench word not found'], Response::HTTP_NOT_FOUND);
    }

}