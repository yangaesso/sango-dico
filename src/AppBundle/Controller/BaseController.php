<?php

namespace AppBundle\Controller;

use MainBundle\Entity\BaseLanguage;
use MainBundle\Entity\Unknown;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

abstract class BaseController extends Controller
{

    const ORIGIN = 'app';
    const FRENCH_IDENTIFIER = 'french_id';
    const USERNAME_APP = 'app';
    const FRENCH_TYPE = [
        'noun'          => 'nom',
        'adjective'     => 'adjectif',
        'pronoun'       => 'Pronom',
        'verb'          => 'verbe',
        'adverb'        => 'adverbe',
        'preposition'   => 'préposition',
        'conjunction'   => 'conjonction',
        'numeral'       => 'Adjectif numéral',
        'determiner'    => 'Adjectif'
    ];

    const SANGO_TYPE = [
        'noun'          => 'pandôo',
        'adjective'     => 'pasûndâ',
        'pronoun'       => 'Polïpa',
        'verb'          => 'Palî',
        'adverb'        => 'mbasêlî',
        'preposition'   => 'Tähüzü',
        'conjunction'   => 'Sëtë',
        'numeral'       => 'pasûndâ',
        'determiner'    => 'pasûndâ',
    ];

    const LINGALA_TYPE = [
        'noun'          => 'nkómbó',
        'adjective'     => 'libákemi',
        'pronoun'       => 'likitana',
        'verb'          => 'likelelo',
        'adverb'        => 'litémele',
        'preposition'   => 'liyamboli',
        'conjunction'   => 'likangisi',
        'numeral'       => 'libákemi',
        'determiner'    => 'libákemi',
    ];

    /***
     * @param $finder
     * @param $word
     * @return bool
     */
    public function checkIfWordRExist($elastica,$word)
    {

        $finder = $this->get($elastica);
        $query = $this->getElasticQuery('word' ,$word);
        $french =  current($finder->find($query));
        if (empty($french)) {

            return false;
        }

        return true;
    }

    /***
     * @param $message
     * @param int $code
     * @return \FOS\RestBundle\View\View Au lieu de renvoyer une réponse JSON, nous allons juste renvoyer une vue FOSRestBundle et laisser le view handler le formater en JSON
     *  Au lieu de renvoyer une réponse JSON, nous allons juste renvoyer une vue FOSRestBundle et laisser le view handler le formater en JSON
     */
    public function errorMessage($message, $code = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        return \FOS\RestBundle\View\View::create([['code'=> $code , 'message' => $message]], Response::HTTP_NOT_FOUND);
    }


    /***
     * get custom query for elasticsearch
     * @param $term
     * @param $source
     * @param $singleRepo
     * @return $this
     */
    public function getElasticQuery($identifier,$term)
    {
        $boolQuery = new \Elastica\Query\BoolQuery();
        $fieldQuery = new \Elastica\Query\Match();


        $fieldQuery->setFieldQuery($identifier, $term);

        $fieldQuery->setFieldParam($identifier, 'analyzer', 'custom_analyzer');

        return $boolQuery->addShould($fieldQuery);
    }
    /***
     * elastic finder
     * @param $index
     * @return object
     */
    public function getElascticFinder($source, $target = null){


        $indexes = ['french' => 'app', 'sango'=>'apps', 'lingala'=>'appl','french_sango'=>'appfs','french_lingala'=>'appfl'];

        $source = strtolower($source);
        $target = strtolower($target);

        if ($source === 'français') {
            $source = 'french';
        }

        if (empty($source)) {
            return null;
        }

        $elasticSource = 'fos_elastica.finder.'.$indexes[$source].'.'.$source;

        if (!empty($target)) {
            $index = 'french_%s';

            if ($source ==='french') {
                $index = sprintf($index,$target);
                $elasticSource = 'fos_elastica.finder.'.$indexes[$index].'.'.$index;
            }else{
                $index = sprintf($index,$source);
                $elasticSource = 'fos_elastica.finder.'.$indexes[$index].'.'.$index;

            }
        }

        return $this->get($elasticSource);
    }

    /**
     * @param string $source
     * @param  string $target
     * @param string $word
     */
    public function setUnknonwWord($source, $target, $word)
    {
        // on renseigne le mot manquant dans la table unknow pour garder unre trace
        $unknown = new Unknown();
        $entity = $this->getDoctrine()->getEntityManager()->getRepository('MainBundle:Unknown')
            ->findBy(array('word' => $word));


        if (empty($entity)) {

            $unknown->setWord($word);
            $unknown->setSource($source);
            $unknown->setTarget($target);
            $unknown->setOrigin(self::ORIGIN);

            $this->getDoctrine()->getEntityManager()->persist($unknown);
            $this->getDoctrine()->getEntityManager()->flush();
        }
    }

    /**
     * @param array $tab
     * @param string $term
     * @return bool
     */
    protected function checkIfTranslationExist( array $tab, $term)
    {
        foreach ($tab as $translation) {
            if ($term === strtolower($translation->getTarget()->getWord())) {
                return true;
            }
        }

        return false;
    }


    protected function getUserApp(): User
    {
        return $this->getDoctrine()->getEntityManager()->getRepository('UserBundle:User')
            ->findOneBy(array('username' => self::USERNAME_APP));
    }

    protected function save($entity): void
    {
        $this->getDoctrine()->getEntityManager()->persist($entity);
        $this->getDoctrine()->getEntityManager()->flush();
    }


    protected function getTranslation(string $identifier, string $searchTerm, string $elasticSearchService)
    {
        //elastic
        $finder = $this->get($elasticSearchService);
        $query = $this->getElasticQuery($identifier, $searchTerm);

        return $finder->find($query);
    }
}
