<?php

namespace AppBundle\Controller;

use MainBundle\Entity\Page;
use MainBundle\Form\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

class PageController extends Controller
{

    /**
     * @Rest\View(serializerGroups={"page"})
     */
    public function getPagesAction(Request $request)
    {
        $pages = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Page')
            ->findAll();

        return $pages;
    }


    /**
     * @Rest\View()
     * @Rest\Get("/page/{id}")
     */
    public function getPageAction( Request $request)
    {
        $page = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Page')
            ->find($request->get('id'));

        /* @var $page Page */

        if (empty($page)) {
            return new JsonResponse(['message' => 'Page not found'], Response::HTTP_NOT_FOUND);
        }

        return $page;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/page")
     */
    public function postPageAction(Request $request)
    {
        $page = new Page();
        $form = $this->createForm(PageType::class, $page);

        /***
         * Donc pour mieux répondre aux contraintes REST, au lieu d’utiliser la méthode handleRequest pour soumettre
         * le formulaire, nous avons opté pour la soumission manuelle avec submit. Nous adaptons Symfony à REST et pas l’inverse.
         */
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($page);
            $em->flush();
            return $page;
        } else {
            return $form;
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/page/{id}")
     */
    public function removePageAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $page = $em->getRepository('MainBundle:Page')
            ->find($request->get('id'));
        /* @var $page Page */

        // Une action idempotente est une action qui produit le même résultat et ce, peu importe le nombre de fois qu’elle est exécutée.
        if ($page){
            $em->remove($page);
            $em->flush();
        }

    }

    /**
     * @Rest\View()
     * @Rest\Put("/page/{id}")
     */
    public function updatePageAction(Request $request)
    {
        $page = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Page')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $page Page */

        if (empty($page)) {
            return new JsonResponse(['message' => 'Page not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(PageType::class, $page);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($page);
            $em->flush();
            return $page;
        } else {
            return $form;
        }

    }

    /***
     * @return static
     *  Au lieu de renvoyer une réponse JSON, nous allons juste renvoyer une vue FOSRestBundle et laisser le view handler le formater en JSON
     */
    private function placeNotFound()
    {
        return \FOS\RestBundle\View\View::create(['message' => 'Place not found'], Response::HTTP_NOT_FOUND);
    }


}
