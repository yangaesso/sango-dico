<?php

namespace AppBundle\Controller;

use MainBundle\Entity\Message;
use MainBundle\Form\MessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

class MessageController extends Controller
{

    /**
     * @Rest\View(serializerGroups={"message"})
     */
    public function getMessagesAction(Request $request)
    {
        return $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Message')
            ->findAll();
    }


    /**
     * @Rest\View()
     * @Rest\Get("/message/{id}")
     */
    public function getMessageAction( Request $request)
    {
        $message = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Message')
            ->find($request->get('id'));

        /* @var $message Message */

        if (empty($message)) {
            return new JsonResponse(['message' => 'Message not found'], Response::HTTP_NOT_FOUND);
        }

        return $message;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/message")
     */
    public function postMessageAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);

        /***
         * Donc pour mieux répondre aux contraintes REST, au lieu d’utiliser la méthode handleRequest pour soumettre
         * le formulaire, nous avons opté pour la soumission manuelle avec submit. Nous adaptons Symfony à REST et pas l’inverse.
         */
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($message);
            $em->flush();
            return $message;
        } else {
            return $form;
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/message/{id}")
     */
    public function removeMessageAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $message = $em->getRepository('MainBundle:Message')
            ->find($request->get('id'));
        /* @var $message Message */

        // Une action idempotente est une action qui produit le même résultat et ce, peu importe le nombre de fois qu’elle est exécutée.
        if ($message){
            $em->remove($message);
            $em->flush();
        }

    }
}
