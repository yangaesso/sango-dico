<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/13/18
 * Time: 11:39 PM
 */

namespace AppBundle\Controller;


use FOS\RestBundle\Request\ParamFetcher;
use MainBundle\Entity\FrenchLingala;
use MainBundle\Entity\Lingala;
use MainBundle\Form\FrenchType;
use phpDocumentor\Reflection\Types\String_;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use MainBundle\Entity\French;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


class FrenchLingalaController extends BaseController
{

    /**
     * Deux query strings vont permettre de choisir l’index du premier résultat souhaité (offset) et le nombre de
     * résultats souhaités (limit).
     *
     * @Rest\View(serializerGroups={"frenchLingala"})

     * @QueryParam(name="offset", requirements="\d+", default="", description="Index de début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="", description="Index de fin de la pagination")
     * @QueryParam(name="sort", requirements="(asc|desc)", nullable=true, description="Ordre de tri (basé sur le nom)")
     *
     * @Rest\Get("/frenchlingala")
     *
     */
    public function getFrenchLingalasAction( ParamFetcher $paramFetcher)
    {

        /***
         * Avec le param fetcher, nous pouvons récupérer nos paramètres et les traiter à notre convenance.
         * Pour gérer la pagination avec Doctrine, nous pouvons utiliser le query builder avec les paramètres offset et limit.
         */
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sort = $paramFetcher->get('sort');

        return $this    ->get('doctrine.orm.entity_manager')
                        ->getRepository('MainBundle:FrenchLingala')
                        ->findTranslationByPagination($offset,$limit,$sort)   ;
    }


    /**
     *
     * @Rest\View(serializerGroups={"frenchLingala"})
     * @Rest\Get("/frenchlingala/{id}")
     */
    public function getFrenchLingalaAction( Request $request)
    {

        //elastic
        $finder = $this->get("fos_elastica.finder.appfl.french_lingala");
        $query = $this->getElasticQuery('id' ,$request->get('id'));

        $frenchLingala =  current($finder->find($query));

        if (empty($frenchLingala)) {
            return $this->errorMessage('FrenchLingala translation not found');
        }

        return $frenchLingala;
    }


    /**
     *
     * @Rest\View(serializerGroups={"frenchLingala"})
     * @Rest\Get("/random/frenchlingala")
     */
    public function getFrenchLingalaRandomAction()
    {

        //elastic
        $finder = $this->get("fos_elastica.finder.appfl.french_lingala");
        $query = $this->getElasticQuery('id' ,rand(1,15000));

        $frenchLingala =  current($finder->find($query));

        if (empty($frenchLingala)) {
            return $this->errorMessage('FrenchLingala translation not found');
        }

        return $frenchLingala;
    }


    /**
     *
     * @Rest\View(serializerGroups={"frenchLingala"})
     * @Rest\Post("/frenchlingala/search")
     */
    public function searchFrenchLingalaAction( Request $request)
    {
        $datas = $request->request->all();
        $search = trim($datas['search']);
        $french = $this->getTranslation($datas['identifier'], $search,'fos_elastica.finder.appfl.french_lingala');

        if (empty($french)) {
            $identifier = substr($datas['identifier'],0,-3);
            $source = $identifier === 'lingala' ? $identifier : 'french';
            $target = $source === 'lingala' ? 'french' :  'lingala';

            $this->setUnknonwWord($source, $target, $search);
            return $this->errorMessage('French Lingala  translation not found');
        }

        return $french;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"frenchLingala"})
     * @Rest\Post("/frenchlingala")
     */
    public function postFrenchLingalaAction(Request $request)
    {
        $frenchManager =  $this->get('main.french_manager');
        $lingalaManager =  $this->get('main.lingala_manager');
        $payload = array_map(
            function ($item){
                return trim(strtolower($item));
            }, $request->request->all()
        );
        $frenchLingala  = $this->getTranslation(self::FRENCH_IDENTIFIER, $payload['french'], 'fos_elastica.finder.appfl.french_lingala');

         if (!empty($frenchLingala) && $this->checkIfTranslationExist($frenchLingala, $payload['traduction'])) {
             return $this->errorMessage("Translation already exist in database", Response::HTTP_CONFLICT);
         }

         if ($payload['check']) {
             if (!$frenchDictionary = $frenchManager->checkDictionary($payload['french'])) {
                 return $this->errorMessage('French word not valid',Response::HTTP_BAD_REQUEST);
             }
         }

         $type = $payload['check'] ? $frenchDictionary->pos : 'noun';
         // on save les mot en database que si ils n'existent pas
         $french = $frenchManager->findTerm($payload['french']);
         if (empty($french)) {
             $french = (new French())
                 ->setWord($payload['french'])
                 ->setLanguage(French::CODE_ISO)
                 ->setType(self::FRENCH_TYPE[$type])
                 ->setStatus(true)
                 ->setUser($this->getUserApp());
             $this->save($french);
         }
        $lingala = $lingalaManager->findTerm($payload['traduction']);

        if (empty($lingala)) {
            $lingala = (new Lingala())
                ->setWord($payload['traduction'])
                ->setLanguage(Lingala::CODE_ISO)
                ->setType(self::LINGALA_TYPE[$type])
                ->setStatus(true)
                ->setUser($this->getUserApp());
            $this->save($lingala);
        }

        $frenchLingala = (new FrenchLingala())
                     ->setFrench($french)
                     ->setLingala($lingala)
                     ->setStatus(false)
                     ->setVotes(0)
                     ->setLikes(0)
                     ->setUser($this->getUserApp());

        $this->save($frenchLingala);

        return $frenchLingala;
    }
}