<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/13/18
 * Time: 11:39 PM
 */

namespace AppBundle\Controller;


use FOS\RestBundle\Request\ParamFetcher;
use MainBundle\Entity\BaseLanguage;
use MainBundle\Form\SangoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use MainBundle\Entity\Sango;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


class SangoController extends BaseController
{

    /**
     * Deux query strings vont permettre de choisir l’index du premier résultat souhaité (offset) et le nombre de
     * résultats souhaités (limit).
     *
     * @Rest\View(serializerGroups={"sango"})

     * @QueryParam(name="offset", requirements="\d+", default="", description="Index de début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="", description="Index de fin de la pagination")
     * @QueryParam(name="sort", requirements="(asc|desc)", nullable=true, description="Ordre de tri (basé sur le nom)")
     *
     */
    public function getSangosAction( ParamFetcher $paramFetcher)
    {
        /***
         * Avec le param fetcher, nous pouvons récupérer nos paramètres et les traiter à notre convenance.
         * Pour gérer la pagination avec Doctrine, nous pouvons utiliser le query builder avec les paramètres offset et limit.
         */
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sort = $paramFetcher->get('sort');

        return $this    ->get('doctrine.orm.entity_manager')
                        ->getRepository('MainBundle:Sango')
                        ->findByPaginattion($offset,$limit,$sort)   ;
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the rewards of an user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Sango::class, groups={"sango"}))
     *     )
     * )
     * @SWG\Parameter(
     *     name="order",
     *     in="query",
     *     type="string",
     *     description="The field used to order rewards"
     * )
     * @SWG\Tag(name="sango")
     *
     * @Rest\View(serializerGroups={"sango"})
     * @Rest\Get("/sango/{id}")
     */
    public function getSangoAction( Request $request)
    {
        $sango = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Sango')
            ->find($request->get('id'));

        //elastic
        $finder = $this->get("fos_elastica.finder.apps.sango");
        $query = $this->getElasticQuery('id' ,$request->get('id'));

        $sango =  current($finder->find($query));


        /* @var $sango Sango */

        if (empty($sango)) {
            return new JsonResponse(['message' => 'Sango word not found'], Response::HTTP_NOT_FOUND);
        }

        return $sango;
    }


    /**
     *
     * @Rest\View(serializerGroups={"sango"})
     * @Rest\Post("/sango/search")
     */
    public function searchSangoAction( Request $request)
    {
        $datas = $request->request->all();
        $finder = $this->get("fos_elastica.finder.apps.sango");

        return $finder->find($datas['search'].'*');
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"sango"})
     * @Rest\Post("/sango")
     */
    public function postSangoAction(Request $request)
    {
        $sango = new Sango();
        $form = $this->createForm(SangoType::class, $sango);

        $payload = $request->request->all();

        if($this->checkIfWordRExist("fos_elastica.finder.apps.sango", $payload['word'])){
            return $this->errorMessage("Word already exist in database");
        };

        /***
         * Donc pour mieux répondre aux contraintes REST, au lieu d’utiliser la méthode handleRequest pour soumettre
         * le formulaire, nous avons opté pour la soumission manuelle avec submit. Nous adaptons Symfony à REST et pas l’inverse.
         */
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($sango);
            $em->flush();
            return $sango;
        } else {
            return $form;
        }
    }

    /**
     * @Rest\View(serializerGroups={"sango"})
     * @Rest\Put("/sango/{id}")
     */
    public function updateSangoAction(Request $request)
    {
        $sango = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Sango')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire


        /* @var $sango Sango */
        if (empty($sango)) {
            $this->sangoNotFound();
        }

        $form = $this->createForm(SangoType::class, $sango);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($sango);
            $em->flush();
            return $sango;
        } else {
            return $form;
        }

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/sango/{id}")
     */
    public function removeSangoAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $sango = $em->getRepository('MainBundle:Sango')
            ->find($request->get('id'));
        /* @var $sango Sango */

        // Une action idempotente est une action qui produit le même résultat et ce, peu importe le nombre de fois qu’elle est exécutée.
        if ($sango){
            $em->remove($sango);
            $em->flush();
        }
    }

    /**
     * @Rest\View()
     * @Rest\Put("/sango/{id}")
     */
    public function updatePlaceAction(Request $request)
    {
        $sango = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:Sango')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $sango Sango */

        if (empty($sango)) {
            return new JsonResponse(['message' => 'Sango not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(SangoType::class, $sango);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($sango);
            $em->flush();
            return $sango;
        } else {
            return $form;
        }

    }

    /***
     * @return static
     *  Au lieu de renvoyer une réponse JSON, nous allons juste renvoyer une vue FOSRestBundle et laisser le view handler le formater en JSON
     */
    private function sangoNotFound()
    {
        return \FOS\RestBundle\View\View::create(['message' => 'Fench word not found'], Response::HTTP_NOT_FOUND);
    }

}