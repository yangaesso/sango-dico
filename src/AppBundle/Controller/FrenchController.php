<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/13/18
 * Time: 11:39 PM
 */

namespace AppBundle\Controller;


use FOS\RestBundle\Request\ParamFetcher;
use MainBundle\Form\FrenchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use MainBundle\Entity\French;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


class FrenchController extends BaseController
{

    /**
     * Deux query strings vont permettre de choisir l’index du premier résultat souhaité (offset) et le nombre de
     * résultats souhaités (limit).
     *
     * @Rest\View(serializerGroups={"french"})

     * @QueryParam(name="offset", requirements="\d+", default="", description="Index de début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="", description="Index de fin de la pagination")
     * @QueryParam(name="sort", requirements="(asc|desc)", nullable=true, description="Ordre de tri (basé sur le nom)")
     *
     */
    public function getFrenchsAction( ParamFetcher $paramFetcher)
    {
        /***
         * Avec le param fetcher, nous pouvons récupérer nos paramètres et les traiter à notre convenance.
         * Pour gérer la pagination avec Doctrine, nous pouvons utiliser le query builder avec les paramètres offset et limit.
         */
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sort = $paramFetcher->get('sort');

        return $this    ->get('doctrine.orm.entity_manager')
                        ->getRepository('MainBundle:French')
                        ->findByPaginattion($offset,$limit,$sort)   ;
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the rewards of an user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=French::class, groups={"french"}))
     *     )
     * )
     * @SWG\Parameter(
     *     name="order",
     *     in="query",
     *     type="string",
     *     description="The field used to order rewards"
     * )
     * @SWG\Tag(name="french")
     *
     * @Rest\View(serializerGroups={"french"})
     * @Rest\Get("/french/{id}")
     */
    public function getFrenchAction( Request $request)
    {
        //elastic
        $finder = $this->get("fos_elastica.finder.app.french");
        $query = $this->getElasticQuery('id' ,$request->get('id'));

        $french =  current($finder->find($query));

        /* @var $french French */

        if (empty($french)) {
            return $this->errorMessage('French word not found');
        }

        return $french;
    }


    /**
     *
     * @Rest\View(serializerGroups={"french"})
     * @Rest\Post("/french/search")
     */
    public function searchFrenchAction(Request $request)
    {
        $datas = $request->request->all();
        //elastic
        $finder = $this->get("fos_elastica.finder.app.french");

        return $finder->find($datas['search'].'*');
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"french"})
     * @Rest\Post("/french")
     */
    public function postFrenchAction(Request $request)
    {
        $french = new French();
        $form = $this->createForm(FrenchType::class, $french);
        $manager =  $this->get('main.french_manager');

        $payload = $request->request->all();

        if($this->checkIfWordRExist("fos_elastica.finder.app.french", $payload['word'])){
           return $this->errorMessage("Word already exist in database");
        };

        if (!$manager->checkDictionary($payload['word'])) {
            return $this->errorMessage('French word not valid');
        }

        /***
         * Donc pour mieux répondre aux contraintes REST, au lieu d’utiliser la méthode handleRequest pour soumettre
         * le formulaire, nous avons opté pour la soumission manuelle avec submit. Nous adaptons Symfony à REST et pas l’inverse.
         */
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($french);
            $em->flush();
            return $french;
        } else {
            return $form;
        }
    }

    /**
     * @Rest\View(serializerGroups={"french"})
     * @Rest\Put("/french/{id}")
     */
    public function updateFrenchAction(Request $request)
    {
        $french = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:French')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire


        /* @var $french French */
        if (empty($french)) {
            return $this->errorMessage('French word not found');        }

        $form = $this->createForm(FrenchType::class, $french);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($french);
            $em->flush();
            return $french;
        } else {
            return $form;
        }

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/french/{id}")
     */
    public function removeFrenchAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $french = $em->getRepository('MainBundle:French')
            ->find($request->get('id'));
        /* @var $french French */

        // Une action idempotente est une action qui produit le même résultat et ce, peu importe le nombre de fois qu’elle est exécutée.
        if ($french){
            $em->remove($french);
            $em->flush();
        }
    }

    /**
     * @Rest\View()
     * @Rest\Put("/french/{id}")
     */
    public function updatePlaceAction(Request $request)
    {
        $french = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MainBundle:French')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $french French */

        if (empty($french)) {
            return $this->errorMessage('French word not found');
        }

        $form = $this->createForm(FrenchType::class, $french);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($french);
            $em->flush();
            return $french;
        } else {
            return $form;
        }
    }
}