<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 11/6/18
 * Time: 4:33 PM
 */
namespace AppBundle\Security;

use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;
use MainBundle\Repository\AuthTokensRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use UserBundle\Repository\UserRepository;

/***
 * Cette classe permettra de récupérer les utilisateurs en se basant sur le token d’authentification fourni.
 *
 * Class AuthTokenUserProvider
 * @package AppBundle\Security
 */
class AuthTokenUserProvider  implements UserProviderInterface
{
    protected $authTokenRepository;
    protected $userRepository;

    public function __construct(AuthTokensRepository $authTokenRepository, UserRepository $userRepository)
    {
        $this->authTokenRepository = $authTokenRepository;
        $this->userRepository = $userRepository;
    }

    public function getAuthToken($authTokenHeader)
    {
        return $this->authTokenRepository->findOneByValue($authTokenHeader);
    }

    /**
     * Loads the user for the given email.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $email The email
     *
     * @return UserInterface
     *
     */
    public function loadUserByUsername($email)
    {
        return $this->userRepository->findOneByEmail($email);
    }

    /**
     * Refreshes the user.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     ** @return UserInterface
     *
     * @throws UnsupportedUserException if the user is not supported
     */
    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
        // Le systéme d'authentification est stateless, on ne doit donc jamais appeler la méthode refreshUser
        throw new UnsupportedUserException();
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return 'UserBundle\Entity\User' === $class;
    }
}