<?php

/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 8/20/17
 * Time: 2:37 PM
 */
namespace UserBundle\Admin;

use UserBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('firstname','text')
            ->add('lastname','text')
            ->add('email','text')
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstname')
            ->add('lastname')
            ->add('email')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('firstname')
            ->addIdentifier('lastname')
            ->addIdentifier('email');
    }

    public function toString($object)
    {
        return $object instanceof User
            ? $object->getLastname()
            : 'Sango'; // shown in the breadcrumb on the create view
    }
}