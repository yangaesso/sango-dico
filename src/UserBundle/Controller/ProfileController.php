<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UserBundle\Controller;

use FOS\UserBundle\Controller\ProfileController as  BaseController;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Event\GetResponseUserEvent;
/**
 * Controller managing the user profile.
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends BaseController
{
    /**
     * Show the user.
     */
    public function showAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Extranet", $this->get("router")->generate("extranet"));
        $breadcrumbs->addItem("Profil utilisateur");

        return parent::showAction();
    }

    /**
     * Edit the user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Request $request)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Extranet", $this->get("router")->generate("extranet"));
        $breadcrumbs->addItem("Editer profil");

        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return parent::editAction($request);
    }
}
