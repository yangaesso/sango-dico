<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user", uniqueConstraints={ @ORM\UniqueConstraint(name="users_email_unique",columns={"email"}) })
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"french","sango","lingala","preference", "auth-token","user"})
     */
    protected  $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", nullable=true)
     * @Serializer\Groups({"french","sango","lingala","user","preference", "auth-token","frenchLingala","frenchSango"})
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", nullable=true)
     * @Serializer\Groups({"french","sango","lingala","user","preference", "auth-token","frenchLingala","frenchSango"})
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable=true)
     * @Serializer\Groups({"user"})
     */
    private $gender;

    /**
     * @var string
     *
     * @Serializer\Groups({"user","preference", "auth-token"})
     */
    protected $username;

    /**
     * @var string
     *
     * @Serializer\Groups({"user","preference", "auth-token"})
     */
    protected $email;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     *
     * @Serializer\Groups({"user"})

     */
    protected $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="string", nullable=true)
     * @Serializer\Groups({"user"})

     */

    private $facebookID;

    /**
     * @var string
     *
     * @ORM\Column(name="google_id", type="string", nullable=true)
     * @Serializer\Groups({"user"})
     */
    private $googleID;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_id", type="string", nullable=true)
     * @Serializer\Groups({"user"})

     */
    private $twitterID;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_acces_token", type="string", nullable=true)
     */
    private $facebookAccessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_acces_token", type="string", nullable=true)
     */
    private $twitterAccessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="google_access_token", type="string", length=255, nullable=true)
     */
    protected $google_access_token;

    /**
     * @var ArrayCollection
     * Many Users have Many sango translations.
     * @ORM\ManyToMany(targetEntity="MainBundle\Entity\FrenchSango", inversedBy="users")
     * @ORM\JoinColumn(name="french_sango_id", referencedColumnName="id",nullable=true)
     */
    private $sango; // Notez le « s », un user est liée à plusieurs translations en sango

    /**
     * @var ArrayCollection
     * Many Users have Many lingala translations.
     * @ORM\ManyToMany(targetEntity="MainBundle\Entity\FrenchLingala", inversedBy="users")
     * @ORM\JoinColumn(name="french_lingala_id", referencedColumnName="id",nullable=true)
     */
    private $lingala; // Notez le « s », un user est liée à plusieurs translations en lingala


    public function eraseCredentials()
    {
        // Suppression des données sensibles
        $this->plainPassword = null;
    }

    public function __construct()
    {
        parent::__construct();
        $this->sango   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lingala = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public  function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);

        return $this;
    }
    /**
     * Get googleID
     *
     * @return int
     */
    public function getGoogleID()
    {
        return $this->googleID;
    }

    /**
     * Get facebookID
     *
     * @return int
     */
    public function getFacebookID()
    {
        return $this->facebookID;
    }

    /**
     * Set facebookID
     *
     * @param string $facebookID
     *
     * @return User
     */
    public function setFacebookID($facebookID)
    {
        $this->facebookID = $facebookID;

        return $this;
    }

    /**
     * Set googleID
     *
     * @param string $googleID
     *
     * @return User
     */
    public function setGoogleID($googleID)
    {
        $this->googleID = $googleID;

        return $this;
    }


    /**
     * Set twitterID
     *
     * @param string $twitterID
     *
     * @return User
     */
    public function setTwitterID($twitterID)
    {
        $this->twitterID = $twitterID;

        return $this;
    }

    /**
     * Get twitterID
     *
     * @return string
     */
    public function getTwitterID()
    {
        return $this->twitterID;
    }

    /**
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Set googleAccessToken
     *
     * @param string $googleAccessToken
     *
     * @return User
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->google_access_token = $googleAccessToken;

        return $this;
    }

    /**
     * Get googleAccessToken
     *
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->google_access_token;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Add sango
     *
     * @param \MainBundle\Entity\FrenchSango $sango
     *
     * @return User
     */
    public function addSango(\MainBundle\Entity\FrenchSango $sango)
    {
        $this->sango[] = $sango;

        return $this;
    }

    /**
     * Remove sango
     *
     * @param \MainBundle\Entity\FrenchSango $sango
     */
    public function removeSango(\MainBundle\Entity\FrenchSango $sango)
    {
        $this->sango->removeElement($sango);
    }

    /**
     * Get sango
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSango()
    {
        return $this->sango;
    }

    /**
     * Set twitterAccessToken
     *
     * @param string $twitterAccessToken
     *
     * @return User
     */
    public function setTwitterAccessToken($twitterAccessToken)
    {
        $this->twitterAccessToken = $twitterAccessToken;

        return $this;
    }

    /**
     * Get twitterAccessToken
     *
     * @return string
     */
    public function getTwitterAccessToken()
    {
        return $this->twitterAccessToken;
    }

    /**
     * Add lingala.
     *
     * @param \MainBundle\Entity\FrenchLingala $lingala
     *
     * @return User
     */
    public function addLingala(\MainBundle\Entity\FrenchLingala $lingala)
    {
        $this->lingala[] = $lingala;

        return $this;
    }

    /**
     * Remove lingala.
     *
     * @param \MainBundle\Entity\FrenchLingala $lingala
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLingala(\MainBundle\Entity\FrenchLingala $lingala)
    {
        return $this->lingala->removeElement($lingala);
    }

    /**
     * Get lingala.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLingala()
    {
        return $this->lingala;
    }
}
