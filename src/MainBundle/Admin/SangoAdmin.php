<?php

/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 8/20/17
 * Time: 2:37 PM
 */
namespace MainBundle\Admin;

use MainBundle\Entity\Sango;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SangoAdmin extends AbstractAdmin
{

    protected $datagridValues = [

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'id',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('word','text')
            ->add('type','choice', [
                'choices' => [
                    'pandôo' => 'pandôo',
                    'pasûndâ' => 'pasûndâ',
                    'Polïpa' => 'Polïpa',
                    'Palî' => 'Palî',
                    'mbasêlî' => 'mbasêlî',
                    'Tähüzü' => 'Tähüzü',
                    'Sëtë' => 'Sëtë',

                ]])
            ->add('url','text', array(
                'required' => false))
            ->add('description','text', array(
                'required' => false))
            ->add('user', 'entity', array(
                'class'    => 'UserBundle:User',
                'choice_label' => 'lastname',))
            ->add('file', 'file', array(
                'required' => false
            ))

        ;
    }



    public function prePersist($sango)
    {
        $this->manageFileUpload($sango);
    }

    public function preUpdate($sango)
    {
        $this->manageFileUpload($sango);
    }

    private function manageFileUpload($sango)
    {

        if ($sango->getFile()) {
            $sango->refreshUpdated();
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('word')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('word')
            ->add('type', 'choice', [
                'choices' => [
                    'pandôo' => 'pandôo',
                    'pasûndâ' => 'pasûndâ',
                    'Polïpa' => 'Polïpa',
                    'Palî' => 'Palî',
                    'mbasêlî' => 'mbasêlî',
                    'Tähüzü' => 'Tähüzü',
                    'Sëtë' => 'Sëtë',

                ],
                'editable'=> true
            ])
            ->addIdentifier('description')
            ->addIdentifier('url')

            ->add('createdAt'     , 'date', array('label' => 'Date de création'))
            ->add('updatedAt'     , 'date', array('label' => 'Date mise à jour'))
            ->addIdentifier('user');
    }

    public function toString($object)
    {
        return $object instanceof Sango
            ? $object->getWord()
            : 'Sango'; // shown in the breadcrumb on the create view
    }
}