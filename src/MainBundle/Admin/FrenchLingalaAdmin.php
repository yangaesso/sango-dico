<?php

/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 8/20/17
 * Time: 2:37 PM
 */
namespace MainBundle\Admin;

use MainBundle\Entity\FrenchLingala;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FrenchLingalaAdmin extends AbstractAdmin
{

    protected $datagridValues = [

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'id',
    ];
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('french', 'entity', array(
                'class'    => 'MainBundle:French',
                'choice_label' => 'word'))
            ->add('lingala', 'entity', array(
                'class'    => 'MainBundle:Lingala',
                'choice_label' => 'word'))

            ->add('votes','text' , array('required' => false))
            ->add('status','text', array('required' => false))
            ->add('likes','text' , array('required' => false))

            ->add('descriptionSource'     , 'text', array('label' => 'Description French' ,'required' => false))
            ->add('descriptionTarget'    , 'text', array('label' => 'Description Lingala','required' => false))

            ->add('user', 'entity', array(
                'class'    => 'UserBundle:User',
                'choice_label' => 'lastname',))

        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('french')
            ->addIdentifier('lingala')
            ->addIdentifier('votes')
            ->add('status','choice', array('label' => 'Statut','editable' => true, 'choices' => array(false => 'non', true => 'Oui')))
            ->addIdentifier('likes')

            ->add('createdAt'     , 'date', array('label' => 'Date de création'))
            ->add('updatedAt'     , 'date', array('label' => 'Date mise à jour'))

            ->add('descriptionSource'     , 'text', array('editable' => true,'label' => 'Description French','required' => false))
            ->add('descriptionTarget'     , 'text', array('editable' => true,'label' => 'Description Lingala','required' => false))

            ->addIdentifier('user');
        ;
    }

    public function toString($object)
    {
        return $object instanceof FrenchLingala
            ? $object->getId()
            : 'FrenchLingala'; // shown in the breadcrumb on the create view
    }
}