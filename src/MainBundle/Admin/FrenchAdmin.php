<?php

/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 8/20/17
 * Time: 2:37 PM
 */
namespace MainBundle\Admin;

use MainBundle\Entity\French;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FrenchAdmin extends AbstractAdmin
{

    protected $datagridValues = [

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'id',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('word','text')
            ->add('type', 'choice', [
                'choices' => [
                    'nom' => 'nom',
                    'adjectif' => 'adjectif',
                    'Pronom' => 'Pronom',
                    'verbe' => 'verbe',
                    'adverbe' => 'adverbe',
                    'préposition' => 'préposition',
                    'conjonction' => 'conjonction',
                    'Adjectif numéral'     => 'Adjectif numéral',
                    'Adjectif'     => 'Adjectif'
                ]])
            ->add('url','text', array(
                'required' => false))
            ->add('description','text', array(
                'required' => false))

            ->add('user', 'entity', array(
                'class'    => 'UserBundle:User',
                'choice_label' => 'lastname',))
            ->add('file', 'file', array(
                'required' => false
            ))

        ;
    }


    public function prePersist($french)
    {
        $this->manageFileUpload($french);
    }

    public function preUpdate($french)
    {
        $this->manageFileUpload($french);
    }

    private function manageFileUpload($french)
    {

        if ($french->getFile()) {
            $french->refreshUpdated();
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('word');
        $datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('word')
            ->add('type', 'choice', [
                                        'choices' => [
                                            'nom' => 'nom',
                                            'adjectif' => 'adjectif',
                                            'Pronom' => 'Pronom',
                                            'verbe' => 'verbe',
                                            'adverbe' => 'adverbe',
                                            'préposition' => 'préposition',
                                            'conjonction' => 'conjonction',
                                            'Adjectif numéral'     => 'Adjectif numéral',
                                            'Adjectif'     => 'Adjectif'
                                        ],
                'editable'=> true
    ])
            ->addIdentifier('description')
            ->addIdentifier('url')

            ->add('createdAt'     , 'date', array('label' => 'Date de création'))
            ->add('updatedAt'     , 'date', array('label' => 'Date mise à jour'))
            ->addIdentifier('user');
    }

    public function toString($object)
    {
        return $object instanceof French
            ? $object->getWord()
            : 'French'; // shown in the breadcrumb on the create view
    }

}