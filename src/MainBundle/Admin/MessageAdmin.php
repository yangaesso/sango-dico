<?php

/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 8/20/17
 * Time: 2:37 PM
 */
namespace MainBundle\Admin;

use MainBundle\Entity\French;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MessageAdmin extends AbstractAdmin
{

    protected $datagridValues = [

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'id',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('firstname','text', array('required' => false, 'label' => 'Prenom'))
            ->add('lastname','text', array('required' => false, 'label' => 'Nom'))
            ->add('email','text', array('required' => false, 'label' => 'Email'))
            ->add('site','choice', [
                'choices' => [
                    'app-bantu-dico'        => 'app-bantu-dico',
                    'web-bantu-dico'        => 'web-bantu-dico',
                    'app-sortir-a-bangui'   => 'app-sortir-a-bangui',
                ]])
            ->add('content', 'sonata_simple_formatter_type',
            array('format' => 'richhtml','ckeditor_context' => 'default'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('site')
            ->add('email')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('site','choice', [
                'choices' => [
                    'app-bantu-dico'        => 'app-bantu-dico',
                    'web-bantu-dico'        => 'web-bantu-dico',
                    'app-sortir-a-bangui'   => 'app-sortir-a-bangui',
                ],
                'editable'=> true])
        ;
    }
}
