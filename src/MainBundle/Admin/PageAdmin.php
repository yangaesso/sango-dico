<?php

/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 8/20/17
 * Time: 2:37 PM
 */
namespace MainBundle\Admin;

use MainBundle\Entity\French;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PageAdmin extends AbstractAdmin
{

    protected $datagridValues = [

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'id',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title'           ,'text'     ,array('required' => false, 'label' => 'Titre'))
        ->add('content', 'sonata_simple_formatter_type',
            array('format' => 'richhtml','ckeditor_context' => 'default'))
        ->add('language','choice', [
            'choices' => [
                'lingala' => 'lingala',
                'sango' => 'sango',
                'french' => 'french',
            ]])

        ;
    }




    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('title')
            ->add('language','choice', [
                'choices' => [
                    'lingala' => 'lingala',
                    'sango' => 'sango',
                    'french' => 'french',
                ],
                'editable'=> true])
        ;
    }


}