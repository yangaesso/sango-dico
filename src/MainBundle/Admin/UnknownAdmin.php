<?php

/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 8/20/17
 * Time: 2:37 PM
 */
namespace MainBundle\Admin;

use MainBundle\Entity\Unknown;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UnknownAdmin extends AbstractAdmin
{

    protected $datagridValues = [

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'id',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('word','text')
            ->add('source','text')
            ->add('target','text')
            ->add('origin','choice', [
                'choices' => [
                    'api' => 'api',
                    'website' => 'website',
                ]])
        ;
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('word')
            ->addIdentifier('source')
            ->addIdentifier('target')
            ->addIdentifier('createdAt')
            ->add('origin','choice', [
                'choices' => [
                    'api'     => 'api',
                    'website' => 'website',
                ],
                'editable'=> true
            ]);
    }

    public function toString($object)
    {
        return $object instanceof Unknown
            ? $object->getId()
            : 'Unknown'; // shown in the breadcrumb on the create view
    }
}