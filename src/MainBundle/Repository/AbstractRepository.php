<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/15/18
 * Time: 8:33 PM
 */

namespace MainBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;




abstract class AbstractRepository extends EntityRepository
{


    /***
     * @param $offset
     * @param $limit
     * @param $sort
     * @return mixed
     */
    public function findByPaginattion($offset, $limit, $sort)
    {

        $offset = !empty($offset) ? $offset : 0;
        $limit  = !empty($limit) ? $limit : 10;
        $sort   = !empty($sort) ? $sort : 'asc';

        $qb = $this ->createQueryBuilder('f')
            ->select('f')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('f.word', $sort);


        return $qb->getQuery()->getResult();
    }

    /***
     * @param $offset
     * @param $limit
     * @param $sort
     * @return mixed
     */
    public function findTranslationByPagination($offset, $limit, $sort)
    {

        $offset = !empty($offset) ? $offset : 0;
        $limit  = !empty($limit) ? $limit : 10;
        $sort   = !empty($sort) ? $sort : 'asc';

        $qb = $this ->createQueryBuilder('f')
            ->select('f')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('f.id', $sort);


        return $qb->getQuery()->getResult();
    }
}