<?php
/**
 * Created by PhpStorm.
 * User: yanga-esso
 * Date: 19/04/17
 * Time: 11:50
 */

namespace MainBundle\Repository;


use FOS\ElasticaBundle\Repository;

class SearchRepository extends Repository
{

    public function findCustom($query)
    {
        // build $query with Elastica objects
        $this->find($query,3,[]);
    }
}