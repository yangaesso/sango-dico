/**
 * Created by yanga-esso on 13/01/17.
 */

$(document).ready(function(){

    /**
     * function  set likes on homepage
     * @param e
     * @returns {boolean}
     */
    function likeHandler(e)
    {
        var clickButton= $(e.target);
        likeUrl = clickButton.data('url');
        console.log(likeUrl);
        $.getJSON( likeUrl , function( data ) {
            clickButton.html(data.likes);
        });
        return false;
    }
    $('#ajaxResult').on("click",".like",likeHandler);


    // switch automatique du sens de traduction au click sur la flèche
    $("img.arrow-trad").click(function () {

        var source = $( "#form_source option:selected" ).text();
        var target = $( "#form_target option:selected" ).text();

        $("#form_target").val(source);
        $("#form_source").val(target);

        return false;
    });

    /*************correct results***********************/

    // function ajaxCorrectCall() {
    //     $.ajax({
    //         url: document.querySelector('.link-correct').getAttribute('data-attr'),
    //         cache: false
    //     })
    //         .done(function (data) {
    //             $("#ajaxVote").html(data).promise().done(
    //                 $('a.link-correct').click(function () {
    //                     ajaxCorrectCall();
    //
    //                     return false;
    //                 }),
    //                 $('a.link-incorrect').click(function () {
    //                     ajaxCorrectCall();
    //                     return false;
    //                 })
    //             );
    //         });
    // }
    //
    // /*************incorrect results***********************/
    // function ajaxIncorrectCall() {
    //     $.ajax({
    //         url: document.querySelector('.link-incorrect').getAttribute('data-attr'),
    //         cache: false
    //     })
    //         .done(function (data) {
    //             $("#ajaxVote").html(data).promise().done(
    //                 $('a.link-correct').click(function () {
    //                     ajaxCorrectCall();
    //
    //                     return false;
    //                 }),
    //                 $('a.link-incorrect').click(function () {
    //                     ajaxIncorrectCall();
    //                     return false;
    //                 })
    //             );
    //         });
    // }
    //
    // $('a.link-correct').click(function () {
    //
    //     ajaxCorrectCall();
    //     return false;
    // });
    //
    // $('a.link-incorrect').click(function () {
    //
    //     ajaxIncorrectCall();
    //     return false;
    // });
    /*************************************************************/

    /*************random results***********************/
    var french = document.querySelector('.french');
    var sango = document.querySelector('.sango');
    var routeRandom = document.querySelector('.random');

    function ajaxCall() {

        $.ajax({
            url: routeRandom.getAttribute('data-attr'),
            cache: false
        })
            .done(function (html) {

                $("#ajaxResult").html(html).promise().done(
                    $('a.random').click(function () {
                        ajaxCall();

                        return false;
                    }),
                    $('a.french').click(function(e) {
                        e.preventDefault();

                        // This next line will get the audio element
                        // that is adjacent to the link that was clicked.
                        var song = $(this).next('audio').get(0);
                        if (song.paused)
                            song.play();
                        else
                            song.pause();
                    }),
                    $('a.sango').click(function(e) {
                        e.preventDefault();

                        // This next line will get the audio element
                        // that is adjacent to the link that was clicked.
                        var song = $(this).next('audio').get(0);
                        if (song.paused)
                            song.play();
                        else
                            song.pause();
                    })

                );
            });
    }

    $('a.random').click(function () {
        ajaxCall();
        return false;
    });
    /*****************************************************/

    //french audio
    $('a.french').click(function(e) {
        e.preventDefault();

        // This next line will get the audio element
        // that is adjacent to the link that was clicked.
        var song = $(this).next('audio').get(0);
        if (song.paused)
            song.play();
        else
            song.pause();
    });

    //sango audio
    $('a.sango').click(function(e) {
        e.preventDefault();

        // This next line will get the audio element
        // that is adjacent to the link that was clicked.
        var song = $(this).next('audio').get(0);
        if (song.paused)
            song.play();
        else
            song.pause();
    });

    /***************multi result***********************************/
    var routeMulti = document.querySelector('.multi');
    function ajaxMultiCall(){
        $.ajax({
            url: routeMulti.getAttribute('data-attr'),
            cache: false
        })
            .done(function (html) {

                $("#ajaxResult").html(html).promise().done(

                    //call pour les results aleatoires
                    $('a.random').click(function () {
                        ajaxCall();

                        return false;
                    }),
                    //call pour audio french
                    $('a.french').click(function(e) {
                        e.preventDefault();

                        // This next line will get the audio element
                        // that is adjacent to the link that was clicked.
                        var song = $(this).next('audio').get(0);
                        if (song.paused)
                            song.play();
                        else
                            song.pause();
                    }),
                    //call pour audio sango
                    $('a.sango').click(function(e) {
                        e.preventDefault();

                        // This next line will get the audio element
                        // that is adjacent to the link that was clicked.
                        var song = $(this).next('audio').get(0);
                        if (song.paused)
                            song.play();
                        else
                            song.pause();
                    }),
                    //call pour les results multi results
                    $('a.multi').click(function () {
                        ajaxMultiCall();

                        return false;
                    })

                );
            });
    }
    $('a.multi').click(function () {
        ajaxMultiCall();
        return false;
    });
});