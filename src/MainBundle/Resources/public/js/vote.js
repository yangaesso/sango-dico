/**
 * Created by yanga-esso on 05/05/17.
 */
/**
 * Created by yanga-esso on 13/01/17.
 */

$(document).ready(function(){

    /*************correct results***********************/

    function ajaxCorrectCall() {
        $.ajax({
            url: document.querySelector('.link-correct').getAttribute('data-attr'),
            cache: false
        })
            .done(function (data) {
                $("#ajaxVote").html(data).promise().done(
                    $('a.link-correct').click(function () {
                        ajaxCorrectCall();

                        return false;
                    }),
                    $('a.link-incorrect').click(function () {
                        ajaxCorrectCall();
                        return false;
                    }),
                    $('a.random').click(function () {
                        ajaxRandomCall();
                        return false;
                    })
                );
            });
    }

    /*************incorrect results***********************/
    function ajaxIncorrectCall() {
        $.ajax({
            url: document.querySelector('.link-incorrect').getAttribute('data-attr'),
            cache: false
        })
            .done(function (data) {
                $("#ajaxVote").html(data).promise().done(
                    $('a.link-correct').click(function () {
                        ajaxCorrectCall();

                        return false;
                    }),
                    $('a.link-incorrect').click(function () {
                        ajaxIncorrectCall();
                        return false;
                    }),
                    $('a.random').click(function () {
                        ajaxRandomCall();
                        return false;
                    })
                );
            });
    }

    $('a.link-correct').click(function () {

        ajaxCorrectCall();
        return false;
    });

    $('a.link-incorrect').click(function () {

        ajaxIncorrectCall();
        return false;
    });
    /*************************************************************/


    /*************random results***********************/
    function ajaxRandomCall() {
        $.ajax({
            url: document.querySelector('.random').getAttribute('data-attr'),
            cache: false
        })
            .done(function (data) {
                $("#ajaxVote").html(data).promise().done(
                    $('a.link-correct').click(function () {
                        ajaxCorrectCall();

                        return false;
                    }),
                    $('a.link-incorrect').click(function () {
                        ajaxIncorrectCall();
                        return false;
                    }),
                    $('a.random').click(function () {
                        ajaxRandomCall();
                        return false;
                    })
                );
            });
    }

    $('a.random').click(function () {

        ajaxRandomCall();
        return false;
    });

    /*************************************************************/


});