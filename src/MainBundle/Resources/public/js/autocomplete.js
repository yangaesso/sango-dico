var cache = {};

$(document).ready(function(){
    $("#search").autocomplete({

        source: function (request, response)
        {
            var objData = {};
            objData = { search: request.term };
            var source =  $("#form_source").val();
            var home = $(this.element).attr('data-url');
            var url = home+'json/search/'+source;
            console.log(url);

            $.ajax({
                url: url,
                dataType: "json",
                data : objData,
                type: 'POST',
                success: function (data)
                {

                    response($.map(data, function (item)
                    {

                        // return '<span>'+ item.word +'</span>  <span class =\"type\">['+ item.language+']</span>';
                        return item.word ;

                    }));
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(textStatus, errorThrown);
                }
            });
        },
        minLength: 3,
        // select: function(event, ui) {
        //     var url = ui.item.id;
        //     if(url != '#') {
        //         location.href = '/blog/' + url;
        //     }
        // },
        select : function(event, ui){ // lors de la sélection d'une proposition
            $('#description').append( ui.item.word ); // on ajoute la description de l'objet dans un bloc
        },
        html: true, // optional (jquery.ui.jquery.ui.autocomplete.html.js required)
        open: function(event, ui) {
            $(".ui-autocomplete").css("z-index", 1000);
        },
        delay: 300
    });
});