<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\PageRepository" )
 */
class Page
{
    const LINGALA = 'lingala';
    const SANGO = 'sango';
    const FRENCH = 'french';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"page"})
     */
    private $id;

    /**
     * @var string
     * @Serializer\Groups({"page"})
     * @ORM\Column(name="title", type="string", length=255, unique=false)
     * @Serializer\Groups({"page"})
     */
    private $title;

    /**
     * @var string
     * @Serializer\Groups({"page"})
     * @ORM\Column(name="language", type="string", length=255, nullable=true)
     * @Serializer\Groups({"page"})
     */
    private $language;

    /**
     * @var string
     * @Serializer\Groups({"page"})
     * @ORM\Column(name="content", type="text")
     * @Serializer\Groups({"page"})
     */
    private $content;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * Set language.
     *
     * @param string $language
     *
     * @return Page
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
