<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
/**
 * Sango
 *
 * @ORM\Table(name="sango")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\SangoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Sango extends BaseLanguage
{
    const CODE_ISO = 'SG';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"french","sango","frenchSango"})
     */
    protected $id;

    /**
     * @Serializer\Groups({"french","sango","frenchSango"})
     */
    protected  $name = 'Sango';

    public function getPath(){

        $path = __DIR__;
        $path = str_replace('Entity','',$path);

        return  $path.'Resources/public/audio/sango/';
    }


    public function getBundlePath(){

        return  '/bundles/main/audio/sango/';
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getName()
    {
        return 'Sango';
    }

}
