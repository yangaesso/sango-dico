<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 11/6/18
 * Time: 3:17 PM
 */

namespace MainBundle\Entity;

/***
 * Class Credentials
 * Cette entité n’aura aucune annotation Doctrine, elle pemettra juste de transporter ces informations ;
 * @package MainBundle\Entity
 */
class Credentials
{
    protected $login;

    protected $password;

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}