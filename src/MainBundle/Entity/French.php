<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

use JMS\Serializer\Annotation as Serializer;

/**
 * French
 *
 * @ORM\Table(name="french")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\FrenchRepository")
 *
 * @ExclusionPolicy("all")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class French extends BaseLanguage
{
    const  CODE_ISO = 'FR';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"french","sango","lingala","frenchSango"})
     *
     * @Expose
     */
    protected  $id;

    /**
     * @Serializer\Groups({"french","sango","lingala","frenchLingala","frenchSango"})
     */
    protected  $name = 'French';
    

    public function getPath(){

        $path = __DIR__;
        $path = str_replace('Entity','',$path);

        return  $path.'Resources/public/audio/french/';
    }

    public function getBundlePath(){

        return  '/bundles/main/audio/french/';
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getName()
    {
        return 'Français';
    }
}
