<?php

namespace MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;
use JMS\Serializer\Annotation as Serializer;


/**
 * frenchSG
 *
 * @ORM\Table(name="french_sango")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\FrenchSangoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class FrenchSango
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"frenchSango"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\French")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Groups({"frenchSango"})
     * @Serializer\SerializedName("source")
     */
    private $french;

    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Sango")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Groups({"frenchSango"})
     * @Serializer\SerializedName("target")
     */
    private $sango;

    /**
     * @var int
     *
     * @ORM\Column(name="Votes", type="integer", nullable=true)
     * @Serializer\Groups({"frenchSango"})
     */
    protected  $votes;


    /**
     * @var boolean
     *
     * @ORM\Column(name="Status", type="boolean")
     * @Serializer\Groups({"frenchSango"})
     */
    protected  $status;

    /**
     * @var int
     *
     * @ORM\Column(name="Likes", type="integer", nullable=true)
     * @Serializer\Groups({"frenchSango"})
     */
    protected  $likes;

    /**
     * @var ArrayCollection
     * Many translations  are voted by many users.
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",mappedBy="sango")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",nullable=true)
     */
    private $users; // Notez le « s », une translation est liée à plusieurs users

    /**
     *  User who have add translation in database
     *  @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",nullable=true)
     *
     * @Serializer\Groups({"frenchSango"})
     */
    protected $user;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     *
     * @Serializer\Groups({"frenchSango"})
     */
    protected  $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime",  nullable=true)
     *
     * @Serializer\Groups({"frenchSango"})
     */
    protected  $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="description_french", type="string", length=255, nullable=true)
     * @Serializer\Groups({"frenchSango"})
     */
    protected  $descriptionSource;

    /**
     * @var string
     *
     * @ORM\Column(name="description_sango", type="string", length=255, nullable=true)
     * @Serializer\Groups({"frenchSango"})
     */
    protected  $descriptionTarget;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set frenchId
     *
     * @param integer $frenchId
     *
     * @return frenchSango
     */
    public function setFrenchId(French $frenchId)
    {
        $this->frenchId = $frenchId;

        return $this;
    }

    /**
     * Get frenchId
     *
     * @return int
     */
    public function getFrenchId()
    {
        return $this->french;
    }

    /**
     * Set sangoId
     *
     * @param integer $sangoId
     *
     * @return frenchSango
     */
    public function setSangoId(Sango $sangoId)
    {
        $this->sangoId = $sangoId;

        return $this;
    }

    /**
     * Get sangoId
     *
     * @return int
     */
    public function getSangoId()
    {
        return $this->sango;
    }


    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user;
    }
    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return user
     */
    public function setUserId(User $user)
    {
        $this->user = $user;

        return $this;
    }


    /**
     * Set votes
     *
     * @param integer $votes
     *
     * @return FrenchSango
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * remove votes
     *
     * @param integer $votes
     *
     *
     */
    public function removeVotes()
    {
        if ($this->getVotes() == 0) {

            return $this;
        }

        --$this->votes;

        return $this;
    }
    /**
     * add votes
     *
     * @param integer $votes
     *
     *
     */
    public function addVotes()
    {
        ++$this->votes;
        return $this;
    }


    /**
     * Get votes
     *
     * @return integer
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set french
     *
     * @param \MainBundle\Entity\French $french
     *
     * @return FrenchSango
     */
    public function setFrench(\MainBundle\Entity\French $french = null)
    {
        $this->french = $french;

        return $this;
    }

    /**
     * Get french
     *
     * @return \MainBundle\Entity\French
     */
    public function getFrench()
    {
        return $this->french;
    }

    /**
     * Get source
     *
     * @return \MainBundle\Entity\French
     */
    public function getSource()
    {
        return $this->french;
    }

    /**
     * Set sango
     *
     * @param \MainBundle\Entity\Sango $sango
     *
     * @return FrenchSango
     */
    public function setSango(\MainBundle\Entity\Sango $sango = null)
    {
        $this->sango = $sango;

        return $this;
    }

    /**
     * Get sango
     *
     * @return \MainBundle\Entity\Sango
     */
    public function getSango()
    {
        return $this->sango;
    }

    /**
     * Get target
     *
     * @return \MainBundle\Entity\Sango
     */
    public function getTarget()
    {
        return $this->sango;
    }

    /**
     * Add user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return FrenchSango
     */
    public function addUser(\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \UserBundle\Entity\User $user
     */
    public function removeUser(\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user id.
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }
	/**
	 * Set status.
	 *
	 * @param bool $status
	 *
	 * @return \MainBundle\Entity\FrenchSango
	 */
	public function setStatus($status)
	{
		$this->status = $status;
		
		return $this;
	}
	
	/**
	 * Get status.
	 *
	 * @return bool
	 */
	public function getStatus()
	{
		return $this->status;
	}
	public function getname(){
		
		 return 'frenchSango';
	}

    /**
     * Set likes
     *
     * @param integer $likes
     *
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;

        return $this;
    }

    /**
     * Get likes
     *
     * @return int
     */
    public function getLikes()
    {
        return $this->likes;
    }



    public function incrementLikes()
    {
        ++$this->likes;
    }



    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return FrenchSango
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return FrenchSango
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getDescriptionSource()
    {
        return $this->descriptionSource;
    }

    /**
     * @param string $descriptionSource
     */
    public function setDescriptionSource($descriptionSource)
    {
        $this->descriptionSource= $descriptionSource;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescriptionTarget()
    {
        return $this->descriptionTarget;
    }

    /**
     * @param string $descriptionTarget
     */
    public function setDescriptionTarget($descriptionTarget)
    {
        $this->descriptionTarget = $descriptionTarget;

        return $this;
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }


}
