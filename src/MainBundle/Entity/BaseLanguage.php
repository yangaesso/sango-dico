<?php
/**
 * Created by PhpStorm.
 * User: yanga-esso
 * Date: 14/01/17
 * Time: 00:37
 */

namespace MainBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UserBundle\Entity\User;
use JMS\Serializer\Annotation as Serializer;


abstract class BaseLanguage
{
    /**
     * @var mixed
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="word", type="string", length=255 )
     * @Serializer\Groups({"french","sango","lingala","frenchLingala","frenchSango"})
     */
    protected  $word;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="string", length=255, nullable=true)
     * @Serializer\Groups({"french","sango","lingala"})
     */
    protected  $description;

    /**
     * @var string
     *
     * @ORM\Column(name="Exemple", type="string", length=255, nullable=true)
     * @Serializer\Groups({"french","sango","lingala"})
     */
    protected  $exemple;


    /**
     * @var string
     *
     * @ORM\Column(name="url",  type="string", length=255, nullable=true)
     * @Serializer\Groups({"french","sango","lingala","frenchLingala","frenchSango"})
     */
    protected  $url;

    /**
     * @var string
     *
     * @ORM\Column(name="Type", type="string",length=30)
     * @Serializer\Groups({"french","sango","lingala","frenchLingala","frenchSango"})
     */
    protected  $type;
    /**
     * @var string
     *
     * @ORM\Column(name="Language", type="string",length=10, nullable=true)
     * @Serializer\Groups({"french","sango","lingala"})
     */
    protected  $language;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Status", type="boolean")
     * @Serializer\Groups({"french","sango","lingala"})
     */
    protected  $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Serializer\Groups({"french","sango","lingala"})
     */
    protected  $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Serializer\Groups({"french","sango","lingala"})
     */
    protected  $updatedAt;



    /**
     * Unmapped property to handle file uploads
     */
    private $file;

    /**
     *  many words can be add by one user
     *  @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",nullable=true)
     * @Serializer\Groups({"french","sango","lingala"})
     */
    protected $user;

    public function __construct()
    {
        $this->status = false;
        $this->likes = 0;
    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {

        $this->file = $file;

    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues



        // move takes the target directory and target filename as params
        $this->getFile()->move(
            $this->getPath(),
            $this->getId().'.mp3'
        );


        $this->setUrl($this->getBundlePath().$this->getId().'.mp3');

        // set the path property to the filename where you've saved the file
        $this->filename = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * Lifecycle callback to upload the file to the server
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function lifecycleFileUpload()
    {

        $this->upload();
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire
     */
    public function refreshUpdated()
    {
        $this->setUpdatedAt(new \DateTime());
    }




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set word
     *
     * @param string $word
     *
     */
    public function setWord($word)
    {
        $this->word = $word;

        return $this;
    }

    /**
     * Get word
     *
     * @return string
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set exemple
     *
     * @param string $exemple
     *
     */
    public function setExemple($exemple)
    {
        $this->exemple = $exemple;

        return $this;
    }

    /**
     * Get exemple
     *
     * @return string
     */
    public function getExemple()
    {
        return $this->exemple;
    }
    /**
     * Set type
     *
     * @param string $type
     *
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set language
     *
     * @param string $language
     *
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }


   /**
     * Set urls
     *
     * @param string $url
     *
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get likes
     *
     * @return int
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Definition
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Definition
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user id.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Definition
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }


    public function incrementLikes()
    {
        ++$this->likes;
    }
	
    /**
     * @return mixed
     */
    public function getUser() {
        return $this->user;
    }

    public function __toString() {
        return $this->word;
    }

}