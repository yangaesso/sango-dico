<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SangoLingala
 *
 * @ORM\Table(name="sango_lingala")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\SangoLingalaRepository")
 */
class SangoLingala
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Sango")
     * @ORM\JoinColumn(nullable=true)
     */
    private $sango;


    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Lingala")
     * @ORM\JoinColumn(nullable=true)
     */
    private $lingala;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sango
     *
     * @param integer $sango
     *
     * @return SangoLingala
     */
    public function setSango($sango)
    {
        $this->sango = $sango;

        return $this;
    }

    /**
     * Get sango
     *
     * @return int
     */
    public function getSango()
    {
        return $this->sango;
    }

    /**
     * Set lingala
     *
     * @param integer $lingala
     *
     * @return SangoLingala
     */
    public function setLingala($lingala)
    {
        $this->lingala = $lingala;

        return $this;
    }

    /**
     * Get lingala
     *
     * @return int
     */
    public function getLingala()
    {
        return $this->lingala;
    }
}

