<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
const NAME = 'Lingala';
/**
 * Lingala
 *
 * @ORM\Table(name="lingala")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\LingalaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Lingala extends BaseLanguage
{
    const  CODE_ISO = 'LG';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"french","lingala","frenchLingala"})
     */
    protected $id;

    /**
     * @Serializer\Groups({"french","lingala","frenchLingala"})
     */
    protected  $name = 'Lingala';


    public function getPath(){

        $path = __DIR__;
        $path = str_replace('Entity','',$path);

        return  $path.'Resources/public/audio/lingala/';
    }

    public function getBundlePath(){

        return  '/bundles/main/audio/french/';
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getName()
    {
        return 'Lingala';
    }
}

