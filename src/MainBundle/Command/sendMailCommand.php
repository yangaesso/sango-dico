<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/29/17
 * Time: 3:38 PM
 */


namespace MainBundle\Command;


use MainBundle\Entity\Unknown;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class sendMailCommand  extends Command
{


    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('mail:send')

            // the short description shown while running "php bin/console list"
            ->setDescription('send daily mail of unknown word')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('this command allow you to send daily mail of unknown word')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container =  $this->getApplication()->getKernel()->getContainer();

        $from = $container->getParameter('contact_email_from');
        $to = $container->getParameter('contact_email_to');
        $subject = 'Mots inconnus de la journée';


        $repository = $container->get('doctrine')->getEntityManager()->getRepository('MainBundle:Unknown');
        $terms  = $repository->findFromLastDay();


        $message = \Swift_Message::newInstance()
            ->setSubject('Recap de la journée')
            ->setFrom($from)
            ->setSubject($subject)
            ->setTo($to)
            ->setBody(
                $container->get('templating')->render('Email/unknown.html.twig',array(
                    'terms'     => $terms,
                )),
                'text/html'
            );


       return  $container->get('mailer')->send($message);
    }
}