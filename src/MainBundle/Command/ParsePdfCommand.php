<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/29/17
 * Time: 3:38 PM
 */


namespace MainBundle\Command;


use Smalot\PdfParser\Parser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParsePdfCommand  extends Command
{


    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('parse:pdf')

            // the short description shown while running "php bin/console list"
            ->setDescription('Parse pde document')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('this command allow you to parse pdf document located at web/updload/document')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $parser = new Parser();
        $document =    __DIR__ ;
        $path = str_replace('src/MainBundle/Command','',$document).'web/upload/dico.pdf';

        $pdf    = $parser->parseFile($path);

        // Retrieve all pages from the pdf file.
        $pages  = $pdf->getPages();

        // Loop over each page to extract text.

        $pages = array_slice($pages,215,1);


        foreach ($pages as $page) {
            $page = $page->getText();

//            $pattern = '/\b[A-Z]+\b/';
//            preg_match_all($pattern,$page,$out,PREG_PATTERN_ORDER);
            dump($page);
        }

        die;
       return   $output->writeln('Parser OK !! La balle est dans ton camp');
    }
}