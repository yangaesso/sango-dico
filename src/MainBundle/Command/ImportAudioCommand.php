<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/29/17
 * Time: 3:38 PM
 */


namespace MainBundle\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportAudioCommand  extends Command
{


    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('audio:import')

            // the short description shown while running "php bin/console list"
            ->setDescription('import audios files in mp3 format')
            ->addArgument('begin', InputArgument::REQUIRED, 'The username of the user.')
            ->addArgument('end', InputArgument::OPTIONAL, 'The username of the user.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('this command allow you to create audio file for word given by its ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $audio = $this->getApplication()->getKernel()->getContainer()->get('main.audio_import');

        $begin = intval($input->getArgument('begin'));
        $end = !empty($input->getArgument('end')) ? intval($input->getArgument('end')): 'end';


         if ( $begin == 0 || !is_string($end) && $end == 0 ) {
             return $output->writeln('les argument doivent être des entiers ! please retry');
         }

         if (!is_string($end) && $end < $begin) {
             return $output->writeln('le deuxieme argument doit etre superieur au premier');
         }

        is_string($end) ? $audio->import($begin): $audio->import($begin,$end);

       return   $output->writeln('Export OK !! La balle est dans ton camp');
    }
}