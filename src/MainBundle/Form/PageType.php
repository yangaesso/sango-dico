<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
class PageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title','text', array('required' => false))
                ->add('language')
                ->add('content', CKEditorType::class, array(
                    'config' => array(
                        'uiColor' => '#ffffff',
                        'toolbar' => 'full',
                    ),
                ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /***
         * Dans une API, il faut obligatoirement désactiver la protection CSRF (Cross-Site Request Forgery).
         * Nous n’utilisons pas de session et l’utilisateur de l’API peut appeler cette méthode sans se soucier de
         * l’état de l’application : l’API doit rester sans état : stateless.
         */
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Page',
            'csrf_protection' => false


        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'main_bundle_page';
    }


}
