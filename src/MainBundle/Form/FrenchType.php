<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FrenchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user')
            ->add('word')
            ->add('description')
            ->add('exemple')
            ->add('url')
            ->add('type')
            ->add('language')
            ->add('status')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\French',
            'csrf_protection' => false

        ));
    }

    public function getBlockPrefix()
    {
        return 'main_bundle_french_type';
    }
}
