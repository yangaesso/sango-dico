<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class MessageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname',TextType::class, array('required' => false))
            ->add('lastname',TextType::class, array('required' => false))
            ->add('email',EmailType::class, array('required' => false))
            ->add('site',EmailType::class, array('required' => true))
            ->add('content',TextareaType::class, array('required' => false))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /***
         * Dans une API, il faut obligatoirement désactiver la protection CSRF (Cross-Site Request Forgery).
         * Nous n’utilisons pas de session et l’utilisateur de l’API peut appeler cette méthode sans se soucier de
         * l’état de l’application : l’API doit rester sans état : stateless.
         */
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Message',
            'csrf_protection' => false


        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'main_bundle_message';
    }


}
