<?php

namespace MainBundle\Manager;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

abstract class BaseManager extends ContainerAwareCommand

{
    /**
     * persist and flush entity
     * @param $entity
     */
    public function persistAndFlush($entity)

    {
        $this->em->persist($entity);
        $this->em->flush();

    }
    /**
     * get firstTerm of entity
     * @param $entity
     */
    public function getfirstTerm($entity)
    {
        return  $this ->em
                      ->getRepository('MainBundle:'.$entity)
                      -> firstTerm();
    }

    /**
     * get firstTerm of entity
     * @param $entity
     */
    public function getlastTerm($entity)
    {
        return  $this ->em
                      ->getRepository('MainBundle:'.$entity)
                      -> lastTerm();
    }

    /**
     * get firstTerm of entity
     * @param $entity
     */
    public function getRandomTerm($entity)
    {
    	
    	if (!$this->getfirstTerm($entity) || !$this->getlastTerm($entity)){
    		return false;
			}
			
			return  $this ->em
										->getRepository('MainBundle:'.$entity)
										-> randomTerm($this->getfirstTerm($entity)->getId(),$this->getlastTerm($entity)->getId());
    }

    /***
     * find term in $entity by id
     * @return mixed
     */
    public function findTermById($entity,$id)
    {
        return $this->em
            ->getRepository('MainBundle:'.$entity)
            ->findOneBy(array('id' => $id));
    }

    /***
     * find term in $entity by id
     * @return mixed
     */
    public function translate($id,$src,$dest)
    {

        $term = array();

        if ($src ==='SG') {

            //recuperation de l'entite source en base
            $result = $this->em
                         ->getRepository('MainBundle:FrenchSango')
                         ->findOneBy(array('sango' => $id));

            $term[] = $result->getFrenchId();

            return  $term;
        }

        $result = $this->em
            ->getRepository('MainBundle:FrenchSango')
            ->findOneBy(array('french' => $id));


        $term[] = $result->getSangoId();

        return  $term;
    }

    /***
     * get custom query for elasticsearch
     * @param $term
     * @param $source
     * @param $singleRepo
     * @return $this
     */
    public function getElasticQueryId($term)
    {
        $boolQuery = new \Elastica\Query\BoolQuery();
        $fieldQuery = new \Elastica\Query\Match();

        $fieldQuery->setFieldQuery('id', $term);

        $fieldQuery->setFieldParam('id', 'analyzer', 'custom_analyzer');

        return $boolQuery->addShould($fieldQuery);
    }

    /***
     * @param $word
     * @return bool|mixed
     */
    public function checkDictionary($word)
    {
        //appel du client guzzle
        $client = new Client();

        $result = $client
            ->request('GET', 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup', [
                'query' => [
                    'key' => 'dict.1.1.20170329T160743Z.80996cc0aae240fc.de9b05ccae3690ae62d989627a479064be4ce229',
                    'lang' => 'fr-fr',
                    'text' => $word
                ]
            ]);

        $result = json_decode((string)($result->getBody()));

        if (empty($result->def)) {
            return false;
        }

        return current($result->def);
    }
}