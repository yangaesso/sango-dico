<?php

namespace MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use MainBundle\Entity\FrenchSango;

class FrenchSangoManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save (FrenchSango $FrenchSango)
    {
        $this->persistAndFlush($FrenchSango);
    }

    public function validateTranslation($valid,$id, $user)
    {
        $sango =  $this->em
                    ->getRepository('MainBundle:FrenchSango')
                    ->find($id);

        /***si l'utilisateur a deja voté pour le mot on ne l'ajoute pas
				 */

        if( !$sango->getUsers()->contains($user)) {
            $sango->addUser( $user);
            if (!$valid) {
                $sango->removeVotes();
            } else {
                $sango->addVotes();

                $user->addSango($sango);

                $this->em->persist($user);
                $this->em->flush();
            }

            $this->save($sango);
        }

    }
    
    public function findEntry($french, $sango)
    {
			  $criteria = array('french' =>$french, 'sango' =>$sango);
			  
        return  $this->em
                    ->getRepository('MainBundle:FrenchSango')
                    ->findOneBy($criteria);
    }
    
    public function getTermsForValidation()
    {
        return  $this->em
                    ->getRepository('MainBundle:FrenchSango')
                    ->getTermsForValidation();
    }
    
    
    public function delete($id)
    {
			$entity = $this->em
								->getRepository('MainBundle:FrenchSango')
								->find($id);
			
    	$this->em->remove($entity);
			$this->em->flush();
			
    }
    
    public function validate($id)
    {
			$entity = $this->em
								->getRepository('MainBundle:FrenchSango')
								->find($id);
			
			$entity->setStatus(true);
			
			$this->save($entity);
		
    }

    public function getTermsForVote(){
	
			return  $this->em
				->getRepository('MainBundle:FrenchSango')
				->getTermsForVote();
		}
		
    public function getRandomTranslation(){

        $result['query']   = $this->getElasticQueryId(rand(1,2000));
        $result['finder']  = "fos_elastica.finder.appfs.french_sango";

        return $result;
    }

    public function getTranslation($id){

			return  $this->em
				->getRepository('MainBundle:FrenchSango')
				->find($id);
    }

	public function getResultsByLanguage($datas,$language){
        $results = array();

        foreach ($datas as $data){

            $lang = $language === 'Sango' ? $data->getSangoId(): $data->getFrenchId();
            $results[] = [
                'id'=> $data->getId(),
                'likes' =>$data->getLikes(),
                'word' =>$lang->getWord(),
                'type' =>$lang->getType(),
                'language' =>$lang->getLanguage()
            ];
        }

        return $results;
    }


    public function getTranslationsByUser($id)
    {
        return  $this->em
            ->getRepository('MainBundle:FrenchSango')
            ->getTranslationsByUser($id);
    }

    /***
     * @param $begin
     * @param $end
     * @return mixed
     */
    public function findWhere($begin,$end)
    {
        return  $this->em
            ->getRepository('MainBundle:FrenchSango')
            ->findWhere($begin,$end) ;
    }

}