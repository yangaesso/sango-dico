<?php

namespace MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use MainBundle\Entity\Sango;

class SangoManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save (Sango $sango)
    {
        $this->persistAndFlush($sango);
    }

    /***
     * get find term
     * @return mixed
     */
    public function findTerm ($term)
    {
        return $this->em
            ->getRepository('MainBundle:Sango')
            ->findOneBy(array('word' => $term));
    }

    public function loadSangoWords($tabs)
    {
        $result = array();

        foreach ($tabs as $tab) {
            $result[] = $tab->getSangoId();
        }

        return $result;
    }

}