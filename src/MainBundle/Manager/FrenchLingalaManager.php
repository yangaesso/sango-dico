<?php

namespace MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use MainBundle\Entity\FrenchLingala;

class FrenchLingalaManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save (FrenchLingala $FrenchLingala)
    {
        $this->persistAndFlush($FrenchLingala);
    }

    public function findEntry($french, $lingala)
    {
        $criteria = array('french' =>$french, 'lingala' =>$lingala);

        return  $this->em
            ->getRepository('MainBundle:FrenchLingala')
            ->findOneBy($criteria);
    }

    public function getResultsByLanguage($datas,$language){
        $results = array();


        foreach ($datas as $data){

            $lang = $language === 'Lingala' ? $data->getLingala(): $data->getFrenchId();
            $results[] = [
                'id'=> $data->getId(),
                'likes' =>$data->getLikes(),
                'word' =>$lang->getWord(),
                'type' =>$lang->getType(),
                'language' =>$lang->getLanguage()
            ];
        }

        return $results;
    }

    public function getTranslation($id){

        return  $this->em
            ->getRepository('MainBundle:FrenchLingala')
            ->find($id);
    }

    public function getRandomTranslation(){

        $result['query']   = $this->getElasticQueryId(rand(10000,15700));
        $result['finder']  = "fos_elastica.finder.appfl.french_lingala";

        return $result;
    }

}