<?php

namespace MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouterInterface;
use MainBundle\Entity\French;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FrenchManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public function getloremIpsum()
    {
        return $this->container->get('apoutchika.lorem_ipsum');
    }

    /***
     * save entity
     */
    public function save (French $french)
    {
        $this->persistAndFlush($french);
    }

    public function findTerm ($term)
    {
        return $this->em
            ->getRepository('MainBundle:French')
            ->findOneBy(array('word' => $term));
    }

    public function loadFrenchWords($tabs)
    {
        $result = array();

        foreach ($tabs as $tab) {
            $result[] = $tab->getFrenchId();
        }

        return $result;
    }
}