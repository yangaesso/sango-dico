<?php

namespace MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use MainBundle\Entity\Page;

class PageManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /***
     * get page by it's title
     * @return mixed
     */
    public function findPageByTitle ($title)
    {
        return $this->em
            ->getRepository('MainBundle:Page')
            ->findOneBy(array('title' => $title));
    }

    /***
     * get page by it's title and language
     * @return mixed
     */
    public function findPageByTitleAndLanguage ($title,$language)
    {
        return $this->em
            ->getRepository('MainBundle:Page')
            ->findOneBy(array('title' => $title,'language'=>$language));
    }

}