<?php

namespace MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use MainBundle\Entity\Lingala;
use MainBundle\Entity\Sango;

class LingalaManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function save (Lingala $lingala)
    {
        $this->persistAndFlush($lingala);
    }

    /***
     * get find term
     * @return mixed
     */
    public function findTerm ($term)
    {
        return $this->em
            ->getRepository('MainBundle:Lingala')
            ->findOneBy(array('word' => $term));
    }

}