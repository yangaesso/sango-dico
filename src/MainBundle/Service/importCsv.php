<?php
/**
 * Created by PhpStorm.
 * User: yanga-esso
 * Date: 05/07/17
 * Time: 14:14
 */

namespace MainBundle\Service;


use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use MainBundle\Entity\French;
use MainBundle\Entity\FrenchLingala;
use MainBundle\Entity\FrenchSango;
use MainBundle\Entity\Lingala;
use MainBundle\Entity\Sango;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\DependencyInjection\Container;

class importCsv {
	
	const LANGUAGE = ['sango'=> 'SG', 'French' => 'FR', 'lingala' => 'LG'];
	
	const FRENCH_TYPE = [
		'noun' => 'nom',
		'adjective' => 'adjectif',
		'pronoun' => 'pronom',
		'verb' => 'verbe',
		'adverb' => 'adverbe',
		'preposition' => 'préposition',
		'conjunction' => 'conjonction',
		'numeral'     => 'adjectif numéral',
		'determiner'     => 'adjectif',
		'particle'     => 'nom'
	];
	
	const SANGO_TYPE = [
		'noun' => 'pandôo',
		'adjective' => 'pasûndâ',
		'pronoun' => 'Polïpa',
		'verb' => 'palî',
		'adverb' => 'mbasêlî',
		'preposition' => 'tähüzü',
		'conjunction' => 'Sëtë',
		'numeral'     => 'pasûndâ',
		'determiner'     => 'pasûndâ',
	];
	
	const LINGALA_TYPE = [
		'noun' => 'nkómbó',
		'adjective' => 'libákemi',
		'pronoun' => 'likitana',
		'verb' => 'likelelo',
		'adverb' => 'litémele',
		'preposition' => 'liyamboli',
		'conjunction' => 'likangisi',
		'numeral'     => 'libákemi',
		'determiner'     => 'libákemi',
	];
	
	
	/**
	 * @var Container
	 */
	private $container;
	private $em;
	private $user;

	public function __construct(Container $container, EntityManager $em)
	{
		$this->container = $container;
		$this->em = $em;
		$this->user = $this->container->get('security.token_storage')->getToken()->getUser();
	}
	
	public function import($file, $datas) {
		$type = 'noun';
		$frenchManager = $this->container->get('main.french_manager');


		$client = new Client();
		$language = $datas['language'];

		//ouverture du fichier en lecture
		$handle = fopen($file, "r");

		while (($data = fgetcsv($handle)) !== FALSE) {

			if (count($data) > 1) {

				//chargement de l'encodage pour prendre en compte les accents

//				$french  = mb_convert_encoding($data[0], "UTF-8", "UTF-16");
//				$targetTerm  = mb_convert_encoding($data[1], "UTF-8", "UTF-16");
                $french  = trim($data[0]);
                $targetTerms  = trim($data[1]);

				// si le mot existe dejà en base c'est qu'il a déjà été validé par l'api
				$frenchTerm = $frenchManager->findTerm($french);

				if (empty($frenchTerm)) {

				    if ($datas['dictionary']) {
                        // le mot n'existe pas en base. appel de l'api pour vérifier si le mot existe dans le dictionnaire
                        $result = $client
                            ->request('GET', 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup', [
                                'query' => [
                                    'key' => 'dict.1.1.20170329T160743Z.80996cc0aae240fc.de9b05ccae3690ae62d989627a479064be4ce229',
                                    'lang' => 'fr-fr',
                                    'text' => $french
                                ]
                            ]);

                        // si le mot existe dans le dictionnaire français
                        if ($result->getStatusCode() === 200) {

                            $result = json_decode((string)($result->getBody()));
                            if (empty($result->def)) {
                                continue;
                            }

                            $def = current($result->def);

                            /****persist new  french word in database
                             * si le mot français existe dejà en base on ne le duplique pas**/


                            $frenchDefinition = new French();
                            $frenchDefinition->setWord($french);
                            $frenchDefinition->setLanguage('FR');
                            $frenchDefinition->setType(self::FRENCH_TYPE[$def->pos]);
                            $frenchDefinition->setStatus(1);
                            $frenchDefinition->setUser($this->user);

                            $frenchManager->save($frenchDefinition);
                            $frenchTerm = $frenchDefinition;
                        } else {
                            //si le mot n'existe pas dans le dictionnaire on passe au mot suivant
                            continue;
                        }

				    }else {
				        //on désactive le dictionnaire

                        $frenchDefinition = new French();
                        $frenchDefinition->setWord($french);
                        $frenchDefinition->setLanguage('FR');
                        $frenchDefinition->setType(self::FRENCH_TYPE['noun']);
                        $frenchDefinition->setStatus(1);
                        $frenchDefinition->setUser($this->user);

                        $frenchManager->save($frenchDefinition);
                        $frenchTerm = $frenchDefinition;
                    }

				}

				/****persist new  sango/Lingala word in database
				 *
				si le mot existe dejà en base on ne le duplique pas. le mot peut etre
				en sango ou en lingala**/


				$this->saveTerms($frenchTerm,$targetTerms,$language,$this->user);


			}
		}

		fclose($handle);
	}

	public function importExcel($file,$datas){

        $client = new Client();
        $language = $datas['language'];
        $type = 'noun';
        $frenchManager = $this->container->get('main.french_manager');
        $spreadsheet = new Spreadsheet();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $spreadsheet = $reader->load($file);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = [];

        foreach ($worksheet->getRowIterator() AS $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];
            foreach ($cellIterator as $cell) {
                $cells[] = $cell->getValue();
            }
            $rows[] = $cells;
        }

        foreach ($rows as $row) {
            $french  = trim($row[0]);
            $targetTerms  = trim($row[1]);

            // si le mot existe dejà en base c'est qu'il a déjà été validé par l'api
            $frenchTerm = $frenchManager->findTerm($french);

            if (empty($frenchTerm)) {

                if ($datas['dictionary']) {
                    // le mot n'existe pas en base. appel de l'api pour vérifier si le mot existe dans le dictionnaire
                    $result = $client
                        ->request('GET', 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup', [
                            'query' => [
                                'key' => 'dict.1.1.20170329T160743Z.80996cc0aae240fc.de9b05ccae3690ae62d989627a479064be4ce229',
                                'lang' => 'fr-fr',
                                'text' => $french
                            ]
                        ]);

                    // si le mot existe dans le dictionnaire français
                    if ($result->getStatusCode() === 200) {

                        $result = json_decode((string)($result->getBody()));
                        if (empty($result->def)) {
                            continue;
                        }

                        $def = current($result->def);

                        /****persist new  french word in database
                         * si le mot français existe dejà en base on ne le duplique pas**/


                        $frenchDefinition = new French();
                        $frenchDefinition->setWord($french);
                        $frenchDefinition->setLanguage('FR');
                        $frenchDefinition->setType(self::FRENCH_TYPE[$def->pos]);
                        $frenchDefinition->setStatus(1);
                        $frenchDefinition->setUser($this->user);

                        $frenchManager->save($frenchDefinition);
                        $frenchTerm = $frenchDefinition;
                    } else {
                        //si le mot n'existe pas dans le dictionnaire on passe au mot suivant
                        continue;
                    }

                }else {
                    //on désactive le dictionnaire

                    $frenchDefinition = new French();
                    $frenchDefinition->setWord($french);
                    $frenchDefinition->setLanguage('FR');
                    $frenchDefinition->setType(self::FRENCH_TYPE['noun']);
                    $frenchDefinition->setStatus(1);
                    $frenchDefinition->setUser($this->user);

                    $frenchManager->save($frenchDefinition);
                    $frenchTerm = $frenchDefinition;
                }

            }

            /****persist new  sango/Lingala word in database
             *
            si le mot existe dejà en base on ne le duplique pas. le mot peut etre
            en sango ou en lingala**/


            $this->saveTerms($frenchTerm,$targetTerms,$language,$this->user);

        }

    }

    /**
     * save terms source and targets
     * @param $source
     * @param $targets
     */
	public function saveTerms($source, $target,$language,$user){

        $manager = 'main.lingala_manager';
        $genericManager = $this->container->get($manager);

        $traductionManager = $this->container->get('main.frenchLingala_manager');

        $targets = explode(",", $target);

        $type = array_search(strtolower($source->getType()),self::FRENCH_TYPE);

        foreach ($targets as $item)
        {

            if (empty($item)){
                continue;
            }
            $term = str_replace("-","",$item);
            $term = trim($term);
            $genericTerm = $genericManager->findTerm($term);


            if (empty($genericTerm)) {


             
                $genericDefinition = new Lingala();

                $genericDefinition->setWord($term);
                $genericDefinition->setLanguage($language);

                if (empty($type)){
                    $type = 'noun';
                }

                $language === 'sango'
                    ? $genericDefinition->setType(self::SANGO_TYPE[$type])
                    : $genericDefinition->setType(self::LINGALA_TYPE[$type]);
                $genericDefinition->setUser($user);

                $genericManager->save($genericDefinition);
                $genericTerm = $genericDefinition;
            }

            //si le mot en sango/lingala exist en base et que la definition en français est differente
            // on ajoute une nouvelle entrée dans la table de traduction

            /****create new entry in  frenchSango tab if not exist   **/

				$entry = $this->container->get('main.frenchLingala_manager')
					->findEntry($source->getId(), $genericTerm->getId());

				if (empty($entry)) {
					$frenchTarget = new FrenchLingala();
                    $frenchTarget->setFrench($source);
                    $frenchTarget->setLingala($genericTerm);
                    $frenchTarget->setStatus(1);
                    $frenchTarget->setLikes(0);
                    $frenchTarget->setUser($user);

                    $traductionManager->save($frenchTarget);
				}

        }

    }
}