<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 7/26/17
 * Time: 11:11 AM
 */

namespace MainBundle\Service;


use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;


class SiteMap
{

    private $container;

    public function __construct( Container $container)
    {

        $this->container = $container;
    }


    /**
     * Génère l'ensemble des valeurs des balises <url> du sitemap.
     *
     * @return array Tableau contenant l'ensemble des balise url du sitemap.
     */
    public function generer()
    {
        $urls = array();
        $host = 'http://'.$this->container->get('router')->getContext()->getHost();
        $em = $this->container->get('doctrine')->getEntityManager();
        $router = $this->container->get('router');
        $slugify = $this->container->get('cocur_slugify');
        $slugify->addRule('é', 'e');
        $slugify->addRule('è', 'e');

        $pages = $em->getRepository('MainBundle:Page')->findAll();

        foreach ($pages as $page) {

            $urls[] = array(
                'loc' => $host.$router->generate('generic_pages', array('title' => $slugify->slugify( $page->getTitle())  ))
            );
        }

        $urls[] = array(
            'loc' => $host.$router->generate('forum')
        );
        $urls[] = array(
            'loc' => $host.$router->generate('contact')
        );
        $urls[] = array(
            'loc' => $host.$router->generate('home')
        );


        return $urls;
    }
}