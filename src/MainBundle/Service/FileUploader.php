<?php
/**
 * Created by PhpStorm.
 * User: yanga-esso
 * Date: 05/07/17
 * Time: 13:41
 */

namespace MainBundle\Service;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class  FileUploader
{
	private $targetDir;
	
	public function __construct($targetDir)
	{
		$this->targetDir = $targetDir;
	}
	
	public function upload(UploadedFile $file, $name)
	{
		$fileName = $name.'.'.$file->guessExtension();
		
		$file->move($this->targetDir, $fileName);
		
		return $this->targetDir.'/'.$fileName;
	}
	
	public function getTargetDir()
	{
		return $this->targetDir;
	}
}