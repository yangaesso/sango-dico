<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/15/17
 * Time: 6:17 PM
 */

namespace MainBundle\Service;

use Doctrine\ORM\EntityManager;

use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\Container;

class importAudio{

    /**
     * @var Container
     */
    private $container;
    private $em;

    public function __construct(Container $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public function import($begin,$end = null) {


        $frenchSango =  $this->em->getRepository('MainBundle:FrenchSango');
        $results = array();

       if( empty($end)) {
           array_push($results,$frenchSango->find($begin));
       }  else {
           $results = $frenchSango->findWhere($begin, $end);
       }


        foreach ($results as $fs) {


            $french = $fs->getFrenchId();
            $sango = $fs->getSangoId();


            $this->download($french,'fr');
            $this->download($sango,'es');

        }


    }

    private function download ($entity,$language){

        $path = $entity->getPath();

        $filename = $file = $entity->getId().'.mp3';

        // Save MP3 file in folder with .mp3 extension
        $file = $path. $filename;

        // verify CHMOD
        if (substr(sprintf('%o', fileperms($path)), -4) != "0777"){
            chmod($path, 0777);
        }

        $client = new Client();

        // If MP3 file exists do not create new request
        if (!file_exists($file)) {
            $mp3 = $client
                ->request('GET', 'http://translate.google.com/translate_tts', [
                    'query' => [
                        'ie' => 'UTF-8',
                        'total' => 1,
                        'idx' => 0,
                        'textlen' => 32,
                        'client' => 'tw-ob',
                        'q' => $entity->getWord(),
                        'tl' => $language,
                    ]
                ]);

            if ($mp3->getStatusCode() === 200) {
                $contents = $mp3->getBody()->getContents();

                file_put_contents($file, $contents);
                chmod($file, 0777);  // notation octale : valeur du mode correcte
            }
        }

        //save url of audio file  in database
        $entity->setUrl($entity->getBundlePath().$entity->getId().'.mp3');
        $this->em->persist($entity);
        $this->em->flush();
    }

}