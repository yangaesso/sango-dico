<?php

/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 8/16/17
 * Time: 1:11 PM
 */
namespace MainBundle\Twig;


class MainExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('setWordBold', array($this, 'setWordBold')),
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('ShareRandomImg', array($this, 'ShareRandomImg')),
        );
    }

    public function ShareRandomImg($begin,$end)
    {
        $number = rand($begin,$end);
        $path = '/bundles/main/imgs/share/social-shared-%d.png';

        $pathImg = sprintf($path,$number);

        return $pathImg;
    }


    public function setWordBold($phrase, $word)
    {
        $position = stripos($phrase,$word);

        if ($position !== false ){

           return $position == 0 ? ucfirst(str_ireplace($word, '<b>'.$word.'</b>', $phrase)):str_ireplace($word, '<b>'.strtolower($word).'</b>', $phrase);
        }
        return $phrase;
    }

}