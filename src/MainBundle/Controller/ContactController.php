<?php
/**
 * Created by PhpStorm.
 * User: yanga-esso
 * Date: 11/04/17
 * Time: 11:12
 */

namespace MainBundle\Controller;

use ReCaptcha\ReCaptcha;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;



class ContactController extends Controller
{
    const SECRET = "6Lfl_zIUAAAAAO8Wy7PR-D_EA-w9xiRJ84002Lr2";
    /***
     * Contact
     *
     *
     */
    public function homeAction(Request $request)
    {
        //add breadcrumb for template
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Contact");
        $user = $this->getUser();

        $data =  array();
        $form = $this ->createFormBuilder($data)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('email', EmailType::class)
            ->add('description', TextareaType::class)
            ->add('submit', SubmitType::class, array('label' => 'Envoyer'))
            ->getForm();

        if ($request->isMethod('POST')) {


           $captcha =  $this->verifyCaptcha(self::SECRET,$_POST['g-recaptcha-response'],$_SERVER['REMOTE_ADDR']);
            if ($captcha['code'] !== 200){
                return $this->render('MainBundle:Contact:contact.html.twig', array(
                    'form' => $form->createView(),'captcha'=> true ));
            }

            $form->handleRequest($request);
            $data = $form->getData();
            $sendMessage = $this->send($data);

            if ($sendMessage) {

                return $this->redirectToRoute('contact_success');
            }
        }

        return $this->render('MainBundle:Contact:contact.html.twig', array(
            'form' => $form->createView() ));
    }

    public function successAction(Request $request)
    {
        return $this->render('MainBundle:Contact:success.html.twig');
    }

    public function send($data)
    {
        // On récupère le service translator
        $translator = $this->get('translator');
        $from = $this->getParameter('mailer_user');
        $to = $this->getParameter('contact_email_to');

        $subject = 'nouveau message de '.$data['firstname'].' '.$data['lastname'];


        $message = \Swift_Message::newInstance()
            ->setSubject($translator->trans('contact.subject'))
            ->setFrom($from)
            ->setSubject($subject)
            ->setTo($to)
            ->setBody(
                $this->renderView('Email/message.html.twig',array(
                    'firstname'     => $data['firstname'],
                    'lastname'      => $data['lastname'],
                    'email'         => $data['email'],
                    'description'   => $data['description'],
                )),
                'text/html'
            )
            ;

      return $this->get('mailer')->send($message);
    }

    public function verifyCaptcha($secret,$gRecaptchaResponse,$remoteIp)
    {
        $recaptcha =  new ReCaptcha($secret);

        $resp = $recaptcha->setExpectedHostname('bantu-dico.com')
            ->verify($gRecaptchaResponse, $remoteIp);

        if ($resp->isSuccess()) {
            return array('code' => 200);
        } else {
            return array('code' => 500);
        }
    }
}
