<?php

namespace MainBundle\Controller;

use MainBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Page controller.
 *
 * @Route("page")
 */
class PageController extends Controller
{
     /**
     * Lists all page entities.
     *
     * @Route("/", name="page_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Extranet", $this->get("router")->generate("extranet"));
        $breadcrumbs->addItem("Administration des pages");

        $em = $this->getDoctrine()->getManager();

        $pages = $em->getRepository('MainBundle:Page')->findAll();

        return $this->render('page/index.html.twig', array(
            'pages' => $pages,
        ));
    }

    /**
     * Creates a new page entity.
     *
     * @Route("/new", name="page_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $page = new Page();
        $form = $this->createForm('MainBundle\Form\PageType', $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush($page);

            return $this->redirectToRoute('page_show', array('id' => $page->getId()));
        }

        return $this->render('page/new.html.twig', array(
            'page' => $page,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a page entity.
     *
     * @Route("/{id}", name="page_show")
     * @Method("GET")
     */
    public function showAction(Page $page)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Extranet", $this->get("router")->generate("extranet"));
        $breadcrumbs->addItem("Pages", $this->get("router")->generate("page_index"));
        $breadcrumbs->addItem("Voir page");
        $deleteForm = $this->createDeleteForm($page);

        return $this->render('page/show.html.twig', array(
            'page' => $page,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing page entity.
     *
     * @Route("/{id}/edit", name="page_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Page $page)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Extranet", $this->get("router")->generate("extranet"));
        $breadcrumbs->addItem("Pages", $this->get("router")->generate("page_index"));
        $breadcrumbs->addItem("Modification de page");

        $deleteForm = $this->createDeleteForm($page);
        $editForm = $this->createForm('MainBundle\Form\PageType', $page);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('page_edit', array('id' => $page->getId()));
        }

        return $this->render('page/edit.html.twig', array(
            'page' => $page,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a page entity.
     *
     * @Route("/{id}", name="page_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Page $page)
    {
        $form = $this->createDeleteForm($page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($page);
            $em->flush($page);
        }

        return $this->redirectToRoute('page_index');
    }

    /**
     * Creates a form to delete a page entity.
     *
     * @param Page $page The page entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Page $page)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('page_delete', array('id' => $page->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function genericAction(Request $request, $title,$language = null)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $navExpression  = array('gbaya','yakoma','ngbaka');
        $navGrammaire   = array('nombres','orthographe','conjugaison');

        switch ( $title) {
            case 'book' :
                $title = "livre d'or";
                break;
            case 'alphabet' :
                $title = "alphabet et phonetique";
                break;
            default:
                $title = $this->get('cocur_slugify')->slugify($title,' ');
        }

        if (empty($language)){
            $page =  $this->get('main.page_manager')->findPageByTitle($title);
        }else{
            $page =  $this->get('main.page_manager')->findPageByTitleAndLanguage($title,$language);

        }
        http://www.torrent9.ec/search_torrent/cpasbien.html

        if (empty($page)) {
            return $this->render('MainBundle:Pages:pageNotFound.html.twig');
        }

        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));

        if (in_array($title,$navExpression)) {
            $breadcrumbs->addItem("Expressions courantes", $this->get("router")->generate("generic_pages",['title' =>'expressions-courantes' ]));
            if (!empty($language)){
                $breadcrumbs->addItem(ucfirst($language));
            }

        }

        if (in_array($title,$navGrammaire)) {

            $breadcrumbs->addItem("Grammaire", $this->get("router")->generate("generic_pages",['title' =>'grammaire','language'=>$language ]));
            if (!empty($language)){
                $breadcrumbs->addItem(ucfirst($language));
            }

        }

        $breadcrumbs->addItem(ucfirst($title));
        if (!empty($language) && !in_array($title,$navGrammaire) ){
            $breadcrumbs->addItem(ucfirst($language));
        }

        return $this->render('MainBundle:Pages:genericPage.html.twig', array('page' => $page));
    }


    public function forumAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem('Forum');


        return $this->render('MainBundle:Pages:forum.html.twig');
    }

    public function donateAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem('Faire un don');


        return $this->render('MainBundle:Pages:donate.html.twig');
    }


}
