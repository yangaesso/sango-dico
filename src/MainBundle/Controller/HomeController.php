<?php

namespace MainBundle\Controller;

use JMS\Serializer\SerializationContext;

use MainBundle\Entity\Unknown;
use MainBundle\Service\readFilter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;


class HomeController extends Controller
{

    const LANGUAGE = array('sango' =>'SG', 'french' => 'FR', 'lingala' => 'LG');
    const ORIGIN = 'website';

    public function indexAction(Request $request, $id, $multi, $source, $target)
    {
        $dataSource = $this->get('session')->get('source');
        $dataTarget = $this->get('session')->get('target');

        $traductionManager = $this->get('main.frenchLingala_manager');

        //random term en FL si user fait des recherches en lingala
        if ((!empty($dataSource) && !empty($dataTarget)) ) {
            if ($dataSource === 'Sango' || $dataTarget === 'Sango' ){

                $traductionManager = $this->get('main.frenchSango_manager');
            }
        }

        if (!empty($source) && !empty($target)) {
            $manager = 'main.'. $this->getTraductionManager(strtolower($source),strtolower($target)).'_manager';
            $traductionManager = $this->get($manager);
        }

        if ($id != null ) {
            $results = array();
            $results[] = $traductionManager->getTranslation($id);

        } else {
            $source = 'Français';
            $target = 'Lingala';
            //get random term
            $results = $this->getRandomTerm();

            $results = !empty($results)? $results: array();

        }

        $data =  array();

        if (!empty($dataSource) && !empty($dataTarget)) {
            $dataSource = ($dataSource === 'french') ? 'Français': $dataSource;
            $dataTarget = ($dataTarget === 'french') ? 'Français': $dataTarget;
        }

        $form = $this ->createFormBuilder($data)
                      ->add('term', TextType::class)
                      ->add('source', ChoiceType::class,
                                array(
                                    'choices' => array('Français'=> 'Français','Sango' => 'Sango','Lingala' => 'Lingala'),
                                    'multiple' => false,
                                    'expanded' => false,
                                    'required' => true,
                                    'data' => !empty($dataSource) ? $dataSource :'Français',
                                ))
                      ->add('target', ChoiceType::class,
                                array(
                                    'choices' => array('Sango' => 'Sango','Français'=> 'Français','Lingala' => 'Lingala'),
                                    'multiple' => false,
                                    'expanded' => false,
                                    'required' => true,
                                    'data' => !empty($dataTarget) ? $dataTarget :'Lingala',
                                ))
                      ->add('save', SubmitType::class, array('label' => 'GO'))
                      ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            $data = $form->getData();
            $data = $this->formatData($data);
            $target = strtolower($data['target']);
            $source = $data['source'];
            $term = trim($data["term"]);

            // set soure term in session
            $session = $this->get('session');
            $session->set('word', array(
                'source' => $term ,
                'language' => $source
            ));

            // set traduction direction in session
            $session->set('source',$data['source'] );
            $session->set('target',$data['target'] );


            //sens de traduction non disponible
            if (empty($this->getTraductionManager(strtolower($data["source"]),strtolower($data["target"])))){

                return $this->render('MainBundle:Home:home.html.twig', array(
                    'form' => $form->createView(), 'results' => array(), 'multi'=> 1));
            }

            $finderSource = $this->getElascticFinder($data["source"]);
            $finderTraduction = $this->getElascticFinder($data["source"],$data["target"]);

            /**on va chercher l'entité correspondant au terme cherché*/


            $query = $this->getElasticQuery('word',$term,$data["source"],true);
            $result =  current($finderSource->find($query));
            // si le mot n'existe pas en base on renvoi un tableau vide
            if (!empty($result)) {


                // find correspondance for translation
                $queryTrad = $this->getElasticQuery('word',$term,$data["source"],false);
                $results = $finderTraduction->find($queryTrad);

                // un mail recap est envoyé par jour
//                if (empty($results)) {
//                    $this->sendMail($term, $data["source"],$data["target"]);
//                }
                //ajout de la langue source si plusieurs resultats
                if ( count($results)>1 ){

                    $results = $this->getResultsByLanguage($results,$data["target"]);
                }


                // si il n'ya pas de traduction pour le mot on renvoi un tableau vide
                if (empty($results)) {

                    $results = array();
                }

            }else{

                // on renseigne le mot manquant dans la table unknow pour garder unre trace
                $unknown = new Unknown();
                $entity = $this->getDoctrine()->getEntityManager()->getRepository('MainBundle:Unknown')
                    ->findBy(array('word' => $term));


                if (empty($entity)) {

                    $unknown->setWord($term);
                    $unknown->setSource($data['source']);
                    $unknown->setTarget($data['target']);
                    $unknown->setOrigin(self::ORIGIN);

                    $this->getDoctrine()->getEntityManager()->persist($unknown);
                    $this->getDoctrine()->getEntityManager()->flush();

//                    $this->sendMail($term, $data["source"],$data["target"]);
                }

                $results = array();
            }

            $multi = null;
        }


        return $this->render('MainBundle:Home:home.html.twig', array(
                                        'form' => $form->createView(),
                                        'results' => $results,
                                        'multi'=> $multi,
                                        'source' => $source,
                                        'target' => $target,
        ));
    }


    /***
     * get custom query for elasticsearch
     * @param $term
     * @param $source
     * @param $singleRepo
     * @return $this
     */
    public function getElasticQuery($identifier, $term, $source,$singleRepo)
    {
        $boolQuery = new \Elastica\Query\BoolQuery();
        $fieldQuery = new \Elastica\Query\Match();

        $field = $singleRepo ? $identifier : strtolower($source).'_id';

        $fieldQuery->setFieldQuery($field, $term);

        $fieldQuery->setFieldParam($field, 'analyzer', 'custom_analyzer');

        return $boolQuery->addShould($fieldQuery);
    }
    /***
     * repertorie les traductions disponibles sur le site
     * @param $source
     * @param $target
     * @return mixed|null
     */
    public function getTraductionManager($source,$target){

        if ($source == $target) {

            return null;
        }

        $managers = ['frenchSango', 'frenchLingala', 'frenchFang'];
        foreach ($managers as $manager) {
            if (strpos(strtolower($manager),$source) !== false && strpos(strtolower($manager),$target) !== false){

                return $manager;
            }
        }

        return null;

    }

    /***
     * elastic finder
     * @param $index
     * @return object
     */
    public function getElascticFinder($source, $target = null){


        $indexes = ['french' => 'app', 'sango'=>'apps', 'lingala'=>'appl','french_sango'=>'appfs','french_lingala'=>'appfl'];

        $source = strtolower($source);
        $target = strtolower($target);

        if ($source === 'français') {
            $source = 'french';
        }

        if (empty($source)) {
            return null;
        }

        $elasticSource = 'fos_elastica.finder.'.$indexes[$source].'.'.$source;

        if (!empty($target)) {
            $index = 'french_%s';

           if ($source ==='french') {
               $index = sprintf($index,$target);
               $elasticSource = 'fos_elastica.finder.'.$indexes[$index].'.'.$index;
           }else{
               $index = sprintf($index,$source);
               $elasticSource = 'fos_elastica.finder.'.$indexes[$index].'.'.$index;

           }
        }

        return $this->get($elasticSource);
    }


    /**
     * les value et label du select doivent etre les meme pour le switch js de la trad french => Français pose probleme
     * @param $data
     * @return mixed
     */
    public function formatData($data){


        if ($data['source'] ===  'Français') {
            $data['source'] = 'french';

            return $data;
        }

        if ($data['target'] ===  'Français') {
            $data['target'] = 'french';

            return $data;
        }

        return $data;

    }
    /**
     * controller for  search autocomplete
     */
    function autocompleteAction(Request $request,$source){


        if($request->isXmlHttpRequest())
        {
            $term = $request->request->get('search');

            // les value et label du select doivent etre les meme pour le switch js de la trad french => Français pose probleme
            if ($source ===  'Français') {
                $source = 'french';
            }
            
            $finder = $this->getElascticFinder($source);
            $context = new  SerializationContext();
            $context->setSerializeNull(true);

            $results = $finder->find('*'.$term.'*');
            $serializer = $this->get('jms_serializer');
            $response = new Response($serializer->serialize($results, 'json'));
            $response -> headers -> set('Content-Type', 'application/json');

            return $response;
        }
    }

    /***
     * controller for like vote
     * @param Request $request
     * @param $id
     * @param $type
     * @return Response
     */
    public function likeAction(Request $request, $id)
    {
        $frenchSangoManager = $this->get('main.frenchSango_manager');

        $translation =   $this ->getDoctrine()
                         ->getEntityManager()
                         ->getRepository('MainBundle:FrenchSango')
                         ->find($id)
        ;


        //is the id already in session
        //in that case we do not increment
        $session = $request->getSession();
        $tabAlreadyVote = $session->get('tabAlreadyVote');
        if ($tabAlreadyVote === null) {
            $tabAlreadyVote = array();
        }

        if (!in_array($id, $tabAlreadyVote)) {
            $translation ->incrementLikes();
            $frenchSangoManager->persistAndFlush($translation );

            $tabAlreadyVote[] = $id;
            $session->set('tabAlreadyVote', $tabAlreadyVote);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode(array('likes' => $translation ->getLikes())));

        return $response;
    }


    public function getRandomTerm()
    {
        $dataSource = $this->get('session')->get('source');
        $dataTarget = $this->get('session')->get('target');

        $traductionManager = $this->get('main.frenchLingala_manager');
        //random term en FL si user fait des recherches en lingala
        if ((!empty($dataSource) && !empty($dataTarget)) ) {
            if ($dataSource === 'Sango' || $dataTarget === 'Sango' ){
                $traductionManager = $this->get('main.frenchSango_manager');
            }
        }

        //get random term
        $results = null;

        while (empty($results)) {
            $datas = $traductionManager->getRandomTranslation();
            $finder = $this->get($datas['finder']);

            $results = $finder->find($datas['query']);
        }


        return $results;
    }
    /***
     * return random word
     * @return Response
     *
     */
    public function randomAction()
    {
        $results = current($this->getRandomTerm());

        return $this->render('MainBundle:Results:result.html.twig',
                                array(
                                    'data' => $results,
                                    'source' =>$results->getSource()->getName(),
                                    'target' =>$results->getTarget()->getName()));
    }

    /***
     * return synonme translation word
     * @return Response
     *
     */
    public function multiAction($id, $source,$target,$term)
    {
        // find correspondance for translation
        $finderTraduction = $this->getElascticFinder($source,$target);
        $results = $finderTraduction->find($term);
        $datas =[];
        foreach ($results as $result){
            if ($result->getTarget()->getId() != $id) {
                array_push($datas, $result);
            }
        }

        // melange les données pour ne pas ressortir la meme traduction
        shuffle($datas);
        $result = $datas[0];

        return $this->render('MainBundle:Results:result.html.twig', array('data' => $result,
                                                                                'multi' => 1,
                                                                                'source' =>$source,
                                                                                'target' =>$target,));
    }

    /***
     * return translate term by id
     * @return Response
     *
     */
    public function TranslateAction($id,$src,$dest)
    {
        $role = 'user';

        $Manager = $this->get('main.french_manager');
        $result = current($Manager->translate($id,$src,$dest));

        $user = $result->getUserId();

        if ($user->hasRole('ROLE_ADMIN')) {
            $role  = 'admin';
        }

        return $this->render('MainBundle:Results:result.html.twig',
            array('data' => $result, 'role' => $role));

    }

    /**
     * Génère le sitemap du site.
     *
     */
    public function siteMapAction()
    {
        return $this->render(
            'MainBundle:Pages:sitemap.xml.twig',
            ['urls' => $this->get('main.sitemap')->generer()]
        );
    }


    public function sendMail($term, $source,$target){

        // On récupère le service translator
        $translator = $this->get('translator');
        $from = $this->getParameter('contact_email_from');
        $to = $this->getParameter('contact_email_to');
        $subject = 'Le mot '.strtoupper($term).' n\'a pas été trouvé en base';

        $message = \Swift_Message::newInstance()
            ->setSubject($translator->trans('contact.subject'))
            ->setFrom($from)
            ->setSubject($subject)
            ->setTo($to)
            ->setBody(
                $this->renderView('Email/unknown.html.twig',array(
                    'term'     => $term,
                    'source'   => $source,
                    'target'   => $target,
                )),
                'text/html'
            );

        return $this->get('mailer')->send($message);

    }

    /**
     * get url for test
     * @param $id
     * @return Response
     *
     */
    public function urlAction()
    {
        $results = [

            "id" => 1,
            "word" => "bonjour",
            "source" => "fr",
            "destination" => "sg",
            "translations" => [
                1  => 'bara',
                2  => 'mbote'
            ],

        ];
        $serializer = $this->container->get('jms_serializer');
        $response = new Response($serializer->serialize($results, 'json'));
        $response -> headers -> set('Content-Type', 'application/json');

        return $response;

    }

    public function getResultsByLanguage($datas,$language){
        $results = array();

        foreach ($datas as $data){

            $lang = $language === 'french' ? $data->getSource(): $data->getTarget();
            $results[] = [
                'id'=> $data->getId(),
                'likes' =>$data->getLikes(),
                'word' =>$lang->getWord(),
                'type' =>$lang->getType(),
                'language' =>$lang->getLanguage()
            ];
        }

        return $results;
    }

}
