<?php
/**
 * Created by PhpStorm.
 * User: yanga-esso
 * Date: 17/02/17
 * Time: 15:55
 */

namespace MainBundle\Controller;

use MainBundle\Entity\French;
use MainBundle\Entity\FrenchLingala;
use MainBundle\Entity\FrenchSango;
use MainBundle\Entity\Lingala;
use MainBundle\Entity\Sango;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GuzzleHttp\Client;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class ExtranetController extends Controller
{

    const LANGUAGE = array('sango' =>'SG', 'french' => 'FR', 'lingala' => 'LG');
		const FRENCH_TYPE = [
			'noun' => 'nom',
			'adjective' => 'adjectif',
			'pronoun' => 'Pronom',
			'verb' => 'verbe',
			'adverb' => 'adverbe',
			'preposition' => 'préposition',
			'conjunction' => 'conjonction',
			'numeral'     => 'Adjectif numéral',
			'determiner'     => 'Adjectif'
		];
		
		const SANGO_TYPE = [
			'noun' => 'pandôo',
			'adjective' => 'pasûndâ',
			'pronoun' => 'Polïpa',
			'verb' => 'Palî',
			'adverb' => 'mbasêlî',
			'preposition' => 'Tähüzü',
			'conjunction' => 'Sëtë',
			'numeral'     => 'pasûndâ',
			'determiner'     => 'pasûndâ',
		];
		
		const LINGALA_TYPE = [
			'noun' => 'nkómbó',
			'adjective' => 'libákemi',
			'pronoun' => 'likitana',
			'verb' => 'likelelo',
			'adverb' => 'litémele',
			'preposition' => 'liyamboli',
			'conjunction' => 'likangisi',
			'numeral'     => 'libákemi',
			'determiner'     => 'libákemi',
		];
		
	public function homeAction(){

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $sangoManager = $this->get('main.sango_manager');
        $frenchSangoManager = $this->get('main.frenchSango_manager');
			
        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Extranet");

        // get random id term in frenchSango entity for vote
        $results = $frenchSangoManager->getTermsForVote();
        shuffle($results);

				if (empty($results)){
					
					return $this->render('MainBundle:Extranet:home.html.twig', array('results' => array()));
				}

			  $translation = $results[0];
				
        return $this->render('MainBundle:Extranet:home.html.twig', array('results' => $translation));
    }


    function addAction(Request $request){
    	
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $frenchManager = $this->get('main.french_manager');


        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Extranet", $this->get("router")->generate("extranet"));
        $breadcrumbs->addItem("Ajout de mot");
        $user = $this->getUser();

        $data =  array();
        $form = $this ->createFormBuilder($data)
            ->add('language', ChoiceType::class, array(
                'choices' => array(
                    'Sango' => 'sango',
                    'Lingala' => 'lingala')))
            ->add('word', TextareaType::class )
            ->add('translate',TextType::class )
            ->add('dictionary', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false,

                ]
            ])
            ->add('submit', SubmitType::class, array('label' => 'VALIDER'))
            ->getForm();

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);
            $data = $form->getData();

            $data['word'] = trim($data['word']);

            $manager = 'main.'.$data['language'].'_manager';
            $genericManager = $this->get($manager);

            $traductionManager = 'main.french'.ucfirst($data['language']).'_manager';
            $frenchTargetManager = $this->get($traductionManager);


            $type = '';

            //appel du client guzzle
            $client = new Client();
            if ($data['dictionary']) {
                $result = $client
                    ->request('GET', 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup', [
                        'query' => [
                            'key' => 'dict.1.1.20170329T160743Z.80996cc0aae240fc.de9b05ccae3690ae62d989627a479064be4ce229',
                            'lang' => 'fr-fr',
                            'text' => $data['translate']
                        ]
                    ]);


                if ($result->getStatusCode() === 200) {

                    $result = json_decode((string)($result->getBody()));

                    if (empty($result->def)) {

                        return $this->redirectToRoute('add_word_no_result');
                    }

                    $def = current($result->def);
                    $type = $def->pos;

                }
            }else{
                // desactivation du dictionnaire
                $type = 'noun';
            }

            /****persist new  french word in database**/
            $frenchTerm = $frenchManager->findTerm($data['translate']);


            if (empty($frenchTerm)) {
                $frenchDefinition = new French();
                $frenchDefinition->setWord($data['translate']);
                $frenchDefinition->setLanguage('FR');
                $frenchDefinition->setType(self::FRENCH_TYPE[$type]);
                $frenchDefinition->setStatus(true);
                $frenchDefinition->setUser($user);

                $frenchManager->save($frenchDefinition);
                $frenchTerm = $frenchDefinition;
            }
                /****persist new  sango/Lingala word in database**/
                $sangoTerm = $genericManager->findTerm($data['word']);

                if (empty( $sangoTerm)){

                    switch ($data['language']) {
                        case 'lingala':
                            $entity = new Lingala();
                            break;
                        default:
                            $entity = new Sango();
                    }

                    $genericDefinition = $entity;

                    $genericDefinition->setWord($data['word']);
                    $genericDefinition->setLanguage(self::LANGUAGE[$data['language']]);

                    $data['language'] === 'sango'
                        ? $genericDefinition->setType(self::SANGO_TYPE[$type])
                        : $genericDefinition->setType(self::LINGALA_TYPE[$type])
                    ;

                    $genericDefinition->setUser($user);

                    $genericManager->save($genericDefinition);
									$sangoTerm = $genericDefinition;
                }

                //si le mot en sango/lingala exist en base et que la definition en français est differente
                // on ajoute une nouvelle entrée en sango/lingala

                /****create new entry in  frenchSango/frenchLingala tab if not exist   **/


                $entry = $frenchTargetManager->findEntry($frenchTerm->getId(),$sangoTerm->getId());

                if (empty($entry)){

                    switch ($data['language']) {
                        case 'lingala':
                            $entity = new FrenchLingala();
                            $target = 'setLingala';
                            break;
                        default:
                            $entity = new FrenchSango();
                            $target = 'setSango';
                    }

                    $frenchTarget = $entity;
                    $frenchTarget->setFrench($frenchTerm);
                    $frenchTarget->$target($sangoTerm);
                    $frenchTarget->setStatus(false);
                    $frenchTarget->setVotes(0);
                    $frenchTarget->setLikes(0);
                    $frenchTarget->setUser($this->getUser());

                    $frenchTargetManager->save($frenchTarget);
                }

                return $this->redirectToRoute('add_word_succes');

        }

        return $this->render('MainBundle:Forms:add.html.twig', array(
            'form' => $form->createView() ));
    }

    function errorAction(Request $request){

        return $this->render('MainBundle:Extranet:error.html.twig');
    }
	
	function importAction(Request $request) {
		
		$breadcrumbs = $this->get("white_october_breadcrumbs");

		
		//add breadcrumb for template
		$breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
		$breadcrumbs->addItem("Extranet", $this->get("router")
			->generate("extranet"));
		$breadcrumbs->addItem("Import CSV");
		$user = $this->getUser();
		
		$data = [];
		$form = $this->createFormBuilder($data)
			->add('language', ChoiceType::class, [
				'choices' => [
					'Sango' => 'sango',
					'Lingala' => 'lingala',
				]
			])
			
			->add('file', FileType::class, array('label' => 'Ficher à importer (CSV file)'))
            ->add('dictionary', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false,

                ]
            ])
			->add('submit', SubmitType::class, ['label' => 'VALIDER'])
			->getForm();
		
		if ($request->isMethod('POST')) {
			
			$form->handleRequest($request);
			$data = $form->getData();

			
			$file = $this->get('main.file_uploader')->upload($data['file'],$data['language']);
			$this->get('main.csv_import')->import($file,$data);





            return $this->redirectToRoute('add_word_succes');
			}
		
		return $this->render('MainBundle:Forms:import.html.twig', array(
			'form' => $form->createView() ));
	}
	
	
	function succesAction(){

        return $this->render('MainBundle:Extranet:succes.html.twig');
    }

	function voteAction($valid,$id,$random){
			$user = $this->getUser();
			$sangoManager = $this->get('main.sango_manager');
			$frenchSangoManager = $this->get('main.frenchSango_manager');
		
		// get random id term in frenchSango entity for vote
		$results = $frenchSangoManager->getTermsForVote();
		shuffle($results);
		
		$translation = $results[0];

			if (!$random) {
					$frenchSangoManager->validateTranslation($valid,$id, $user);
			}

			return $this->render('MainBundle:Extranet:vote.html.twig', array('data' => $translation));

	}
  
  function validationAction(Request $request)
	{
		$breadcrumbs = $this->get("white_october_breadcrumbs");
		//add breadcrumb for template
		$breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
		$breadcrumbs->addItem("Extranet", $this->get("router")->generate("extranet"));
		$breadcrumbs->addItem("validation de mots");
		
		$paginator  = $this->get('knp_paginator');
		$frenchSangoManager = $this->get('main.frenchSango_manager');
		
		$query  =  $frenchSangoManager->getTermsForValidation();
		
		$pagination = $paginator->paginate(
			$query, /* query NOT result */
			$request->query->getInt('page', 1)/*page number*/,
			10/*limit per page*/
		);
		
		
		return $this->render('MainBundle:Extranet:validation.html.twig',array('pagination' => $pagination));
		
	}

	
	public function validateTranslationAction($id,$entity)
	{
		$manager = 'main.' . $entity . '_manager';
		$this->get($manager)->validate($id);
		
		
		return $this->redirectToRoute('term_validation');
	}
	
	
	public function deleteTranslationAction($id,$entity)
	{
		$manager = 'main.' . $entity . '_manager';
		$this->get($manager)->delete($id);
		
		return $this->redirectToRoute('term_validation');
	}


    public function listAction(Request $request)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        //add breadcrumb for template
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Extranet", $this->get("router")->generate("extranet"));
        $breadcrumbs->addItem("Mes traductions");

        $frenchSangoManager = $this->get('main.frenchSango_manager');
        $paginator  = $this->get('knp_paginator');

        $query  =  $frenchSangoManager->getTranslationsByUser($this->getUser()->getId());

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );


        return $this->render('MainBundle:Extranet:list.html.twig',array('pagination' => $pagination));
    }

}
