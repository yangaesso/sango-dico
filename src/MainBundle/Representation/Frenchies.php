<?php
/**
 * Created by PhpStorm.
 * User: yanga
 * Date: 10/15/18
 * Time: 9:18 PM
 */

namespace MainBundle\Representation;

use JMS\Serializer\Annotation\Type;
use Pagerfanta\Pagerfanta;

class Frenchies
{
    /**
     * Nous avons ajouté que l'annotation  ("array<AppBundle\Entity\Article>")
     * afin d'indiquer au serializer que ce qui est contenu dans la propriété$ meta est un ArrayCollection
     *
     * @Type("array<MainBundle\Entity\French>")
     */
    public $data;


    public $meta;

    public function __construct(Pagerfanta $data)
    {
        $this->data = $data;

        $this->addMeta('limit', $data->getMaxPerPage());
        $this->addMeta('current_items', count($data->getCurrentPageResults()));
        $this->addMeta('total_items', $data->getNbResults());
        $this->addMeta('offset', $data->getCurrentPageOffsetStart());
    }

    public function addMeta($name, $value)
    {
        if (isset($this->meta[$name])) {
            throw new \LogicException(sprintf('This meta already exists. You are trying to override this meta, use the setMeta method instead for the %s meta.', $name));
        }

        $this->setMeta($name, $value);
    }

    public function setMeta($name, $value)
    {
        $this->meta[$name] = $value;
    }
}