<?php

namespace MainBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MainBundle\Entity\Lingala;
use MainBundle\Entity\Sango;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadLingalaData extends AbstractFixture  implements FixtureInterface, ContainerAwareInterface,OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function load(ObjectManager $manager)
    {
        $loremIpsum = $this->container->get('apoutchika.lorem_ipsum');

        for ($i =1 ; $i < 100 ; $i ++) {
            $definition = new Lingala();
            $definition->setDescription('description '.$loremIpsum->getWords(7));
            $definition->setExemple('exemple '.$loremIpsum->getWords(11));
            $definition->setWord($loremIpsum->getWords(2));
            $definition->setLikes($i);
            $definition->setType('pandôo');
            $definition->setLanguage('SG');
            $definition->setStatus(1);
            $definition->setVotes(10);
            $definition->setVotes(10);
            $definition->setUser($this->getReference('admin'));
            $manager->persist($definition);
            $manager->flush();
        }

    }
    public function getOrder()
    {
        return 4;
    }
}