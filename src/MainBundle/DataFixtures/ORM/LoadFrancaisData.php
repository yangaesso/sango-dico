<?php

namespace MainBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MainBundle\Entity\French;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class LoadFrancaisData extends AbstractFixture  implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $loremIpsum = $this->container->get('apoutchika.lorem_ipsum');

        for ($i =1 ; $i < 100 ; $i ++) {
            $definition = new French();
            $definition->setDescription('description '.$loremIpsum->getWords(7));
            $definition->setExemple('exemple '.$loremIpsum->getWords(11));
            $definition->setWord($loremIpsum->getWords(2));
            $definition->setLikes($i);
            $definition->setLanguage('FR');
            $definition->setType('adj');
            $definition->setStatus(1);
            $definition->setVotes(10);
            $definition->setUser($this->getReference('admin'));

            $manager->persist($definition);
            $manager->flush();
        }

    }
    public function getOrder()
    {
        return 2;
    }
}